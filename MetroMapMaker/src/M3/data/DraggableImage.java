/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.data;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Willliam
 */
public class DraggableImage extends ImageView implements Draggable {
    double startX;
    double startY;
    double lastX;
    double lastY;
    Integer mouseOffsetX = null;
    Integer mouseOffsetY = null;
    private String url;

    
    public DraggableImage(String url){
        super(url);
        this.url = url;
    }
    public DraggableImage(Image image, String url){
        super(image);
        this.url = url;
    }
   
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
        lastX = getX();
        lastY = getY();
    }
    
    @Override
    public void drag(int x, int y) {
        double diffX = x - startX ;
	double diffY = y - startY;
	double newX = getX()+ diffX;
	double newY = getY()+ diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }
    
   
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getX();
	setFitWidth(width);
	double height = y - getY();
	setFitHeight(height);	
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
	setFitWidth(initWidth);	
	setFitHeight(initHeight);	
    }
    
    @Override
    public String getShapeType() {
	return IMAGE;
    }

    @Override
    public double getWidth() {
        return getImage().getWidth();
    }

    @Override
    public double getHeight() {
        return getImage().getHeight();
    }
    
     @Override
    public double getLastX() {
	return lastX;
    }

    @Override
    public double getLastY() {
	return lastY;
    }
    
     @Override
    public void setX(int x) {
        xProperty().set(x);
    }

    @Override
    public void setY(int y) {
	yProperty().set(y);
    }
    
    public String getURL(){
        return url;
    }
    
    @Override
    public Draggable clone(){
        DraggableImage cloneImage = new DraggableImage(this.getImage(), url);
        cloneImage.xProperty().set(xProperty().get());
	cloneImage.yProperty().set(yProperty().get());
        return cloneImage;
    }
}
