package M3.data;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author William Braxton
 * @version 1.0
 */
public interface Draggable {
    public static final String STATION = "STATION";
    public static final String LINE = "LINE";
    public static final String IMAGE = "IMAGE";
    public static final String TEXT = "TEXT";
    public static final String LINE_END = "LINE_END";
    //public M3State getStartingState();
    public void start(int x, int y);
    public void drag(int x, int y);
    public void size(int x, int y);
    public double getX();
    public double getY();
    public void setX(int x);
    public void setY(int y);
    public double getLastX();
    public double getLastY();
    public double getWidth();
    public double getHeight();
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight);
    public String getShapeType();
    public Draggable clone();
}
