/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.data;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 *
 * @author William
 */
public class MetroLine extends Path{
    LineEnd startSeg;
    LineEnd endSeg;
    private Boolean isCircular;
    private String lineName;
    private List<MetroStation> stations;

    public MetroLine(String name, Color lineColor, LineEnd startSeg, LineEnd endSeg ){
        lineName = name;
        //Start segment
        this.setStroke(lineColor);
        this.startSeg = startSeg;
        this.endSeg = endSeg;
        
        startSeg.centerXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            checkCircular();
        });
        
        startSeg.centerYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            checkCircular();
        });
        
        endSeg.centerXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            checkCircular();
        });
        
       endSeg.centerYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
           checkCircular();
        });
        checkCircular(); //Just in case we are being initialized from a saved file
        this.startSeg.setLine(this);
        this.endSeg.setLine(this);
        stations = new ArrayList<>();
        draw();

       
    }
    
    /**
     * This method will draw the path. It always start with the first segment, then ends with
     * the last one. Lines will be drawn to each part of the StattionArray.
     */
    private void draw(){
        getElements().clear();
        //Start with the lineStart
        MoveTo startPoint = new MoveTo();
        startPoint.xProperty().bind(startSeg.centerXProperty());
        startPoint.yProperty().bind(startSeg.centerYProperty());
        getElements().add(startPoint);
        
        stations.stream().map((currentStation) -> {
            LineTo stationPoint = new LineTo();
            stationPoint.xProperty().bind(currentStation.centerXProperty());
            stationPoint.yProperty().bind(currentStation.centerYProperty());
            return stationPoint;
        }).forEachOrdered((stationPoint) -> {
            getElements().add(stationPoint);
        });
        //Now the end point!
        LineTo endPoint = new LineTo();
        endPoint.xProperty().bind(endSeg.centerXProperty());
        endPoint.yProperty().bind(endSeg.centerYProperty());
        getElements().add(endPoint);
    }
    
    /**
     * This will add the stations to line. It will find its place by comparing this
     * stations distance with that of every other stations. It also adds itself to 
     * the list of lines the station is apart of
     * @param station 
     */
    public void addStation(MetroStation station){
        Point2D stationPoint = new Point2D(station.getX(), station.getY()); //This is the station to be added coordinates.
        int closestStationIndex;
        double closestStationDistance;
        int secondClosestStationIndex;
        double secondClosestStationDistance;
        double calculatedDistance;

        if (stations.isEmpty()){
            stations.add(station);
        }
        else{
            MetroStation currentStation = stations.get(0);
            closestStationDistance = stationPoint.distance(currentStation.getX(), currentStation.getY());
            secondClosestStationDistance = stationPoint.distance(currentStation.getX(), currentStation.getY());
            closestStationIndex = 0;
            secondClosestStationIndex = 0;
            for (int i = 1; i < stations.size(); i++){ //compare distance for each station
                currentStation = stations.get(i);
                calculatedDistance = stationPoint.distance(currentStation.getX(), currentStation.getY());
                if (calculatedDistance < closestStationDistance){ //Check if this is closer than the closest position
                    //Reassign closest station to second closest station
                    secondClosestStationDistance = closestStationDistance;
                    secondClosestStationIndex = closestStationIndex;
                    //make current statio the closest station
                    closestStationDistance = calculatedDistance;
                    closestStationIndex = i;
                }
                else if(calculatedDistance < closestStationDistance){
                    secondClosestStationDistance = calculatedDistance;
                    secondClosestStationIndex = i;
                }
            }
            //check the end points
            Double startDist = stationPoint.distance(startSeg.getX(),startSeg.getY());
            Double endDist = stationPoint.distance(endSeg.getX(),endSeg.getY());
            if(startDist < closestStationDistance || startDist < secondClosestStationDistance ){
                stations.add(0,station);
            }
            else if(endDist< closestStationDistance || endDist < secondClosestStationDistance ){
                stations.add(station);
            }
            else if (Math.abs(closestStationIndex - secondClosestStationIndex) == 1){
                int pos = (closestStationIndex + secondClosestStationIndex)/2;
                stations.add(pos+1, station);
            }
            else{
                stations.add(closestStationIndex, station);
            }
            
        }
        station.addMetroLine(this);
        draw();
    }
    
    /**
     * This will add the stations to line. It will find its place by comparing this
     * stations distance with that of every other stations. It also adds itself to 
     * the list of lines the station is apart of. THIS WILL NOT ADD THE LINE TO THE STATION
     * @param station 
     */
  
    
    /**
     * Check if a line is circular or not. if it is,
     * snap the line segments together
     */
    public final void checkCircular(){
        if (startSeg.contains(endSeg.getCenterX(), endSeg.getCenterY())){
            startSeg.setCenterX(endSeg.getCenterX());
            startSeg.setCenterY(endSeg.getCenterY());
            isCircular = true;
        }
        else{
            isCircular = false;
        }
        
    }
    
    
    public Boolean isCircular(){
        return isCircular;
    }
    
    public void setCircular(boolean circular){
        isCircular = circular;
        if(isCircular == true){
            startSeg.setCenterX(endSeg.getCenterX());
            startSeg.setCenterY(endSeg.getCenterY());
        }
    }
    /**
     * In order to remove a line, it must be prepped to be removed. This consist of
     * removing all station on the line from the line
     */
    public void processRemoval(){
        stations.forEach((station) -> {
            station.removeMetroLine(this);
        });
        getElements().clear();
    }
    
    /**
     * This removes a station from the line, it also removes itself from the list
     * of lines that the station is apart of
     * @param station 
     */
    public void removeStation(MetroStation station){
        stations.remove(station);
        station.removeMetroLine(this);
        draw();
    }
    
    
    /**
     * This is to be used when we dont want to remove the line from the station
     * this is to be used when the station is going to be deleted, as to not interere
     * @param station 
     */
    public void removeStationOnly(MetroStation station){
        stations.remove(station);
        draw();
    }
    
    public void setLineColor(Color lineColor){
        this.setStroke(lineColor);
    }
    
    public Color getLineColor(){
        return (Color) this.getStroke();
    }
    
    public void setStationList(List<MetroStation> stations){
        this.stations = stations;
        stations.forEach((station) -> {
            station.addMetroLine(this);
        });
        draw();
    }
    
    /**
     * to be used by transaction handler. This only adds stations to lines and 
     * not lines to stations
     * @param stations 
     */
    public void setStationListOnly(List<MetroStation> stations){
        this.stations = stations;
        draw();
    }
    
    public List getStationList(){
        return stations;
    }
    public String getLineName(){
        return lineName;
    }
    
    public void setLineName(String name){
        lineName = name;
        startSeg.setLineName(name);
        endSeg.setLineName(name);

    }
    
    public LineEnd getStartSegment(){
        return startSeg;
    }
    
    public LineEnd getEndSegment(){
        return endSeg;
    }
    
    @Override
    public String toString(){
        return lineName;
    }
    
    @Override
    public Draggable clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
