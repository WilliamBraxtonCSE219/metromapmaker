/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.data;

/**
 *
 * @author Willliam
 */

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
        
public class DraggableText extends Text implements Draggable {
    int startX;
    int startY;
    double lastX;
    double lastY;
    Boolean boldProperty = false;
    Boolean italicsProperty = false;
    String fontType = "Times New Roman";
    String fontSize = "12";
    
    public DraggableText(int x, int y, String text){
           super(x,y,text);
    }
    public DraggableText(String text){
           super(text);
    }
    
    public M3State getStartingState() {
        return M3State.STARTING_TEXT;
        
    }
   
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
        lastX = getX();
        lastY = getY();
    }
    
    @Override
    public void drag(int x, int y) {
        double diffX = x - startX ;
	double diffY = y - startY;
	double newX = getX()+ diffX;
	double newY = getY()+ diffY;
        //setLayoutX(newX);
	//setLayoutY(newY);
        setX(newX);
        setY(newY);    
	startX = x;
	startY = y;
    }
    
    /**
     * This is our method that will change the font type for the text. It will be
     * invoked anytime something changes
     * @param font
     * @param size
     * @param isBold
     * @param isItalics 
     */
    private void changeFont(String font, String size, Boolean isBold, Boolean isItalics ){
        FontWeight boldType = isBold ? FontWeight.BOLD : FontWeight.NORMAL;
        FontPosture italicsType = isItalics ? FontPosture.ITALIC : FontPosture.REGULAR;
        double newFontSize = Double.parseDouble(fontSize);
        Font textFont = Font.font(fontType, boldType, italicsType, newFontSize);
        this.setFont(textFont);
    }
    
    public void setBoldProperty(Boolean boldSet){
        boldProperty = boldSet;
        changeFont(fontType, fontSize, boldProperty, italicsProperty);
    }
    
    public void setItalicsProperty(Boolean italicsSet){
        italicsProperty = italicsSet;
        changeFont(fontType, fontSize, boldProperty, italicsProperty);
    }
    
    public void setFontType(String fontType) {
        this.fontType = fontType;
        changeFont(this.fontType, fontSize, boldProperty, italicsProperty);
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize; 
        changeFont(fontType, this.fontSize, boldProperty, italicsProperty);
    }
    
    public Boolean getBoldProperty(){
        return boldProperty;
    }
    
    public Boolean getItalicsProperty(){
        return italicsProperty;
    }
    
    public String getFontType(){
        return fontType;
    }
    
    public String getFontSize(){
        return fontSize;
    }
   
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getLayoutX();
	double height = y - getLayoutY();
        this.getLayoutBounds();
	//setFitHeight(height);	
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	//this.getLayoutBounds (initWidth);	
	//setFitHeight(initHeight);
        setX(initX);
        setY(initY);
    }
    
    @Override
    public String getShapeType() {
	return TEXT;
    }

    @Override
    public double getWidth() {
        return this.getLayoutBounds().getWidth();
    }

    @Override
    public double getHeight() {
        return this.getLayoutBounds().getHeight();
    }
     @Override
    public double getLastX() {
	return lastX;
    }

    @Override
    public double getLastY() {
	return lastY;
    }
    
   
    public Draggable clone(){
        DraggableText cloneText = new DraggableText((int)getX(),(int)getY(), this.getText());
        cloneText.setFontSize(fontSize);
        cloneText.setBoldProperty(boldProperty);
        cloneText.setFontType(fontType);
        cloneText.setItalicsProperty(italicsProperty);
        return cloneText;
    }

    @Override
    public void setX(int x) {
        super.setX(x);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
    }
}

