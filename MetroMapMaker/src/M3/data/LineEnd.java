/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.data;

import static M3.data.Draggable.STATION;
import java.util.List;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

/**
 *This class is the line segments for the poly line. The idea is that the line will be binded to 
 * this center property on the ends, allowing for drag use
 * @author William
 */
public class LineEnd extends Ellipse {
    private double startCenterX;
    private double startCenterY;
    private double lastX;
    private double lastY;
    private MetroLine line;
    private DraggableEndText lineName;

    
    public LineEnd(int x, int y) {
        setCenterX(x);
	setCenterY(y);
	setRadiusX(5.0);
	setRadiusY(5.0);
	setOpacity(0.0);
	startCenterX = 0.0;
	startCenterY = 0.0;
        lineName = new DraggableEndText("init");
        positionText();
    }
    
   
    
    public final void positionText(){
        lineName.xProperty().bindBidirectional(this.centerXProperty());
        lineName.yProperty().bindBidirectional(this.centerYProperty());
    }
    
    public void setLine(MetroLine line){
        this.line = line;
        lineName.setText(line.getLineName());
        lineName.setLine(line);
    }
    
    public Text getText(){
        return lineName;
    }
    
    
    public MetroLine getLine(){
        return line;
    }
    
    public String getLineName(){
        return lineName.getText();
    }
    
    public void setLineName(String text){
         lineName.setText(text);
         
    }
    
    public void setText(String text){
        lineName.setText(text);
    }
    
    @Override
    public String toString(){
        return lineName.getText();
    }
    
   
    public double getX() {
	//return getCenterX() - getRadiusX();
	return getCenterX();
    }

    public double getY() {
	//return getCenterY() - getRadiusY();
	return getCenterY();
    }
    
    
    public void setX(int x) {
        setCenterX(x);
    }

    public void setY(int y) {
	setCenterY(y);
    }
    
    

    @Override
    public Draggable clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
