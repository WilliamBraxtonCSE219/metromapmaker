package M3.data;

import M3.data.Draggable;
import static M3.data.Draggable.STATION;
import M3.data.M3State;
import M3.data.MetroLine;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

/**
 * This is a draggable ellipse for our goLogoLo application.
 * 
 *
 * @author William Braxton
 * @version 1.0
 */
public class MetroStationTest extends Pane implements Draggable {
    private double startX;
    private double startY;
    private double lastX; //Used for transaction
    private double lastY;
    private Text stationName;
    private MetroLine line;
    private int labelPos; 
    private Ellipse stationElip;
    public MetroStationTest(String stationName) {
        setTranslateX(100);
        setManaged(false);
        stationElip = new Ellipse();
	stationElip.setRadiusX(5.0);
	stationElip.setRadiusY(5.0);
	stationElip.setOpacity(1.0);
        this.stationName = new Text(stationName);
        
        this.getChildren().addAll(this.stationElip,this.stationName);
        line = null;
        labelPos = 0;
        positionLabel();
    }
    
    
    
    public final void positionLabel(){
        switch(labelPos){
            case 0: //0 IS UPPER RIGHT
                stationName.setX(stationElip.getCenterX() + 20);
                stationName.setY(stationElip.getCenterY() - 20);
                break;
            case 1://1 IS LOWER RIGHT
                stationName.setX(stationElip.getCenterX() + 20);
                stationName.setY(stationElip.getCenterY() + 20);
                break;
            case 2://2 IS LOWER LEFT
                stationName.setX(stationElip.getCenterX() - 20);
                stationName.setY(stationElip.getCenterY() + 20);
                break;
            case 3://3 IS UPPER LEFT
                stationName.setX(stationElip.getCenterX() - 20);
                stationName.setY(stationElip.getCenterY() - 20);
                break;
        } 
    }
    
    public void rotateLabel(){
        if (labelPos == 3){
            labelPos = 0;
        }
        else{
            labelPos += 1;
        }
       
    }
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
        lastX = getX();
        lastY = getY();
        
    }
    
    public MetroLine getMetroLine(){
        return line;
    }
    
    public void setMetroLine(MetroLine line){
        this.line = line;
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x - startX ;
	double diffY = y - startY;
	double newX = getX()+ diffX;
	double newY = getY()+ diffY;
        setX((int) newX);
        setY((int) newY);    
	startX = x;
	startY = y;
        
    }
    
   
    @Override
    public void size(int x, int y) {
	double width = x - startX;
	double height = y - startY;
	double centerX = startX + (width / 2);
	double centerY = startY + (height / 2);
        setX(x);
        setY(y);
	//stationElip.setCenterX(centerX);
	//stationElip.setCenterY(centerY);
	//stationElip.setRadiusX(width / 2);
	//stationElip.setRadiusY(height / 2);	
	
    }
        
   
    
    @Override
    public void setX(int x) {
        this.setLayoutX(x);
    }

    @Override
    public void setY(int y) {
      this.setLayoutY(y);
    }
    
    
    @Override
    public double getLastX() {
	return lastX;
    }
    /**
     * setLayoutY(in y)
     * Sets the pane layout for X not Y
     * It is a fature not a bug
     * @return 
     */
    @Override
    public double getLastY() {
	return lastY;
    }

  
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setX((int) (initX + (initWidth/2)));
	setY((int) (initY + (initHeight/2)));
	setX((int) (initWidth/2));
	setY((int) (initHeight/2));
    }
    
    @Override
    public String getShapeType() {
	return STATION;
    }
    
    
     @Override
    public Draggable clone(){
        MetroStationTest cloneStation = new MetroStationTest(stationName.getText());
       // cloneStation.setFill(getFill());
        //cloneStation.setStroke(getStroke());
        //cloneStation.setCenterX(getCenterX());
	//cloneStation.setCenterY(getCenterY());
	//cloneStation.setRadiusX(getRadiusX());
	//cloneStation.setRadiusY(getRadiusX());
        //cloneStation.setMetroLine(line);
        return cloneStation;
    }

    @Override
    public double getX() {
        return this.getLayoutX();
      
    }

    @Override
    public double getY() {
        return this.getLayoutY();
    }
    
}
