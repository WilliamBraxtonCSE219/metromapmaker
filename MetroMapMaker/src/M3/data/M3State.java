
package M3.data;

/**
 * This enum has all the possible states that M3 can be in
 * @author William
 */
public enum M3State {
    SELECTING_NODE,
    DRAGGING_NODE,
    DRAGGING_NOTHING, 
    STARTING_IMAGE, 
    STARTING_TEXT, 
    STARTING_LINE,
    STARTING_STATION,
    ADDING_TO_LINE,
    REMOVING_FROM_LINE
}
