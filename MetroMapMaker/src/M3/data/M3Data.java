/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.data;

import static M3.M3LangaugeProperty.INPUT_TEXT_CONTENT;
import static M3.M3LangaugeProperty.INPUT_TEXT_HEADER;
import static M3.M3LangaugeProperty.INPUT_TEXT_TITLE;
import static M3.data.M3State.SELECTING_NODE;
import M3.dialog.EditLineSingleton;
import M3.dialog.AddStationDialogSingleton;
import M3.gui.M3Workspace;
import M3.transactions.AddLineTransaction;
import M3.transactions.AddNodeTransaction;
import M3.transactions.AddStationTransaction;
import M3.transactions.ChangeBackgroundTransaction;
import M3.transactions.ChangeFontTransaction;
import M3.transactions.ColorChangeTransaction;
import M3.transactions.EditStationTransaction;
import M3.transactions.RemoveNodeTransaction;
import M3.transactions.ShapeEditTransaction;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.ui.AppMessageDialogSingleton;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.util.Pair;
import jtps.jTPS;

/**
 *
 * @author William Braxton
 */
public class M3Data implements AppDataComponent {
 // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    
    // THESE ARE THE NODES 
    ObservableList<Node> mapNodes;
    ObservableList<Node> stations;
    ObservableList<Node> lines;

    Color backgroundColor;
    String backgroundImage;

    // THIS IS THE NODE CURRENTLY SELECTED
    Node selectedNode;
    MetroStation lastSelectedStation;
    MetroLine lastSelectedLine;

    //TRANSACTION HANDLER
    jTPS transactionHandler;
    // CURRENT STATE OF THE APP
    M3State state;
    
    Group grid;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    // USE THIS WHEN THE NODE IS SELECTED
    Effect highlightedEffect;

    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;
     /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public M3Data(AppTemplate initApp) {
	// KEEP THE APP FOR LATER
	app = initApp;
        
	// NO SHAPE STARTS OUT AS SELECTED
	selectedNode = null;

        //MAKE OUR TRANSACTION HANDLER
        transactionHandler = new jTPS();
        backgroundColor = (Color) DEFAULT_BACKGROUND_COLOR;
        backgroundImage = "";
        stations = FXCollections.observableArrayList();
        lines = FXCollections.observableArrayList();
	// THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(15);
	highlightedEffect = dropShadowEffect;
    }
    
    //GETTERS AND SETTERS
    
    public jTPS getTransactions(){
        return transactionHandler;
    }
    
    public M3State getState() {
	return state;
    }
    
    public MetroLine getSelectedMetroLine(){
        return lastSelectedLine;
    }
    
    public MetroStation getSelectedstation(){
        return lastSelectedStation;
    }

    public void setState(M3State initState) {
	state = initState;
    }
    
    public void setShapes(ObservableList<Node> initShapes) {
	mapNodes = initShapes;
    }
    
    public void setStations(ObservableList<Node> initShapes) {
	stations = initShapes;
    }
    
    public void setLines(ObservableList<Node> initShapes) {
	lines = initShapes;
    }
    
    public ObservableList<Node> getShapes(){
        return mapNodes;
    }
    
    public ObservableList<Node> getLines(){
        return lines;
    }
    
    public ObservableList<Node> getStations(){
        return stations;
    }
    
    public Node getSelectedNode(){
        return selectedNode; 
    }
    
    public Color getBackgroundColor() {
	return backgroundColor;
    }
    
    public String getBackgroundImage(){
        return backgroundImage;
    }
    
    public void setBackgroundColor(Color initBackgroundColor) {
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        ChangeBackgroundTransaction transaction = new ChangeBackgroundTransaction(initBackgroundColor, 
                backgroundImage, canvas, this);
        transactionHandler.addTransaction(transaction);
	/*BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
	Background background = new Background(fill);
	canvas.setBackground(background);*/
    }
    public void setBackgroundColorProperty(Color initBackgroundColor){
        backgroundColor = initBackgroundColor;
    }
    
     public void setBackgroundImageProperty(String imageUrl){
        backgroundImage = imageUrl;
    }
    
    public void setBackgroundImage(String imageUrl) {
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        ChangeBackgroundTransaction transaction = new ChangeBackgroundTransaction(backgroundColor, imageUrl, canvas, this);
        transactionHandler.addTransaction(transaction);
        //ChangeBackgroundTransaction transaction = new ChangeBackgroundTransaction(initBackgroundColor, canvas);
        //transactionHandler.addTransaction(transaction);
	/*BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
	Background background = new Background(fill);
	canvas.setBackground(background);*/
    }
    
    //ADD METHODS
    public void addLine(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        //implement dialog to ask for line name
        EditLineSingleton addLineDialog = EditLineSingleton.getSingleton();
        Optional<Pair<Pair<String,Boolean>, Color>> result = addLineDialog.showAndWait();
        if(result.isPresent() && !result.get().getKey().equals("")){
            LineEnd startSeg = new LineEnd(10,10);
            LineEnd endSeg = new LineEnd(50,50);
            MetroLine newLine = new MetroLine(result.get().getKey().getKey(), result.get().getValue(), 
                    startSeg, endSeg);
            newLine.setCircular(result.get().getKey().getValue());
            AddLineTransaction transaction = new AddLineTransaction(newLine, lines, mapNodes, 
                    AddLineTransaction.ADD);
            transactionHandler.addTransaction(transaction);
             //select the shape!
            if(selectedNode != null){
                unhighlightShape(selectedNode);
                selectedNode = null;
            }
            workspace.getMetroLinesChoiceBox().getSelectionModel().select(newLine);
            selectedNode = newLine;
            lastSelectedLine = newLine;
            highlightShape(selectedNode);
        }
    }
    
    public void addStation() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        //implement dialog to ask for station name
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        AddStationDialogSingleton stationDialog = AddStationDialogSingleton.getSingleton();
        Optional<String> result = stationDialog.showAndWait();
        if (result.isPresent() && !result.get().equals("")){
            Boolean duplicate = false;
            for (Node station : stations){
                MetroStation currentStation = (MetroStation) station;
                if(currentStation.getStationName().equals(result.get())){
                    duplicate = true;
                }
            }
            if (!duplicate){
                MetroStation newStation = new MetroStation(result.get());
                AddStationTransaction transaction = new  AddStationTransaction(newStation,stations, mapNodes, 
                        AddStationTransaction.ADD);
                transactionHandler.addTransaction(transaction);
                //select the shape!
                if(selectedNode != null){
                    unhighlightShape(selectedNode);
                    selectedNode = null;
                }
                
                workspace.getMetroStationsChoiceBox().getSelectionModel().select(newStation);
                selectedNode = newStation;
                lastSelectedStation = newStation;
                highlightShape(selectedNode);
                //ADD IT TO THE COMBOBOX
            }
            else{
                dialog.show("Duplicate Station", result.get() + " Already Exists");
            }
        }
    }
        
    public void addImage(DraggableImage newImage){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        
        newImage.start(0,0);
 
        if (selectedNode != null) {
	    unhighlightShape(selectedNode);
	    selectedNode = null;
	}
        // ADD THE IMAGE TO THE CANVAS
       AddNodeTransaction transaction = new AddNodeTransaction(newImage, mapNodes);
       transactionHandler.addTransaction(transaction);        
    }
    
    public void addText(){
        TextInputDialog textInput = new TextInputDialog();
        textInput.setTitle(app.getProperty(INPUT_TEXT_TITLE));
        textInput.setHeaderText(app.getProperty(INPUT_TEXT_HEADER));
        textInput.setContentText(app.getProperty(INPUT_TEXT_CONTENT));
        Optional<String> result = textInput.showAndWait();
        if (!result.get().trim().equals("")){ //Make sure input is not blank
            DraggableText newText = new DraggableText(100,100,result.get());
            newText.start(100,100);
            if (selectedNode != null) {
                unhighlightShape(selectedNode);
                selectedNode = null;
            }
            selectedNode = newText;
            highlightShape(selectedNode);
            M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
            AddNodeTransaction transaction = new AddNodeTransaction((Node)newText, mapNodes);
            transactionHandler.addTransaction(transaction);
        }
    
    }
    
    //THIS SECTION IS FOR EDITING MAP ELEMENTS
    
    /**
     * Boldens or unboldens selected text
     */
    public void ToggleTextBold(){
        if (selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText) selectedNode;
            //Flip the logic of the bold statement
            ChangeFontTransaction transaction = new ChangeFontTransaction(text, ChangeFontTransaction.BOLD);
            transactionHandler.addTransaction(transaction);
           
        }
    }
    
    /**
     * Toggles the text italics property
     */
    public void ToggleTextItalics(){
        if (selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText) selectedNode;
            //Flip the logic of the bold statement (Done in transaction)
            ChangeFontTransaction transaction = new ChangeFontTransaction(text, ChangeFontTransaction.ITALICS);
            transactionHandler.addTransaction(transaction);
            //text.setItalicsProperty(!text.getItalicsProperty());
        }
    }
    
    public void ChangeTextFamily(String textFamily){
          if (selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText) selectedNode;
            //Flip the logic of the bold statement
            if (!text.getFontType().equals(textFamily) && !textFamily.equals("")){
                ChangeFontTransaction transaction = new ChangeFontTransaction(text,textFamily, ChangeFontTransaction.FONT);
                transactionHandler.addTransaction(transaction);
            }
          
            //text.setFontType(textFamily);
        }
    }
    
    public void ChangeTextSize(String fontSize ){
        if (selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText) selectedNode;
            //Flip the logic of the bold statement
            if (!text.getFontSize().equals(fontSize)&& !fontSize.equals("")){ //check if it was really an action or change
                ChangeFontTransaction transaction = new ChangeFontTransaction(text,fontSize, 
                        ChangeFontTransaction.SIZE);
                transactionHandler.addTransaction(transaction);
            }
        }       
    }
    
    public void changeFontColor(Color fontColor){
         if (selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText) selectedNode;
            
            ColorChangeTransaction transaction = new ColorChangeTransaction(text, fontColor);
            transactionHandler.addTransaction(transaction);
        }
    }
    
    public void changeStationColor(Color stationColor){
        if (selectedNode instanceof MetroStation){
            MetroStation station = (MetroStation) lastSelectedStation;
            
            ColorChangeTransaction transaction = new ColorChangeTransaction(station, stationColor);
            transactionHandler.addTransaction(transaction);
        }
    }
    
    public void makeGrid(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        if(grid != null){
            mapNodes.remove(grid);
            grid.getChildren().clear();
        }
        if(grid ==null){
            grid = new Group();
        }
        for(int i = 0; i <= canvas.getHeight(); i+= 20){
                Line gridLine = new Line(0,i,canvas.getWidth(),i);
                grid.getChildren().add(gridLine);
            }
            for(int i = 0; i <= canvas.getWidth(); i+= 20){
                Line gridLine = new Line(i,0,i,canvas.getHeight());
                grid.getChildren().add(gridLine);
            }
            mapNodes.add(0,grid);
    }

    public void toggleGrid(boolean selected){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        if (grid == null){
            grid = new Group();
            makeGrid();
        }
        makeGrid();
        grid.setVisible(selected);
        
    }
    //REMOVE METHODS
    /**
     * This removes a line
     */
    public void removeLine(){
        if (lastSelectedLine != null){
            //Prep the line for removal
            if (lastSelectedLine == selectedNode){
                unhighlightShape(selectedNode);
                selectedNode = null;
            }
            List<MetroStation> list = lastSelectedLine.getStationList();
            AddLineTransaction transaction = new AddLineTransaction(lastSelectedLine, lines, mapNodes, AddLineTransaction.REMOVE);
            transactionHandler.addTransaction(transaction);
           
            
            //make the last selected line null
            lastSelectedLine = null;
        }
    }
    
    public void removeStation(){
        if (lastSelectedStation != null){
            //prep the station for removal
            if (lastSelectedStation == selectedNode){
                unhighlightShape(selectedNode);
                selectedNode = null;
            }
            
            AddStationTransaction transaction = new  AddStationTransaction(lastSelectedStation, stations, mapNodes, 
                        AddStationTransaction.REMOVE);
            transactionHandler.addTransaction(transaction);
            //Make the last selected station null
           
            lastSelectedStation = null;
        }
    }
    
    public void removeNode(){
        if (selectedNode != null || selectedNode instanceof DraggableImage ||
               selectedNode instanceof DraggableText) {
            RemoveNodeTransaction transaction = new RemoveNodeTransaction((Node)selectedNode, mapNodes);
            transactionHandler.addTransaction(transaction);
	    unhighlightShape(selectedNode);
	    selectedNode = null;
	}
    }
    
    public boolean isInState(M3State testState) {
	return state == testState;
    }
    
    public void selectStation(Node node){
         M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
         MetroStation station = (MetroStation) node;
         for(Node currentStation : stations){
             if(station == currentStation){
                 if (selectedNode != null) {
                    unhighlightShape(selectedNode);
                    }
                 selectedNode = currentStation;
                 lastSelectedStation = (MetroStation) currentStation;
                 highlightShape(currentStation);
             }
         }
    }
    
    public void selectLine(Node node){
         M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
         MetroLine line = (MetroLine) node;
         for(Node currentLine : lines){
             if(line == currentLine){
                 if (selectedNode != null) {
                    unhighlightShape(selectedNode);
                    }
                 selectedNode = currentLine;
                 lastSelectedLine = (MetroLine) currentLine;
                 highlightShape(currentLine);
             }
         }
    }
    
    public Node selectTopNode(int x, int y) {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Node shape = getTopNode(x, y);
	if (shape == selectedNode)
	    return shape;
	
	if (selectedNode != null) {
	    unhighlightShape(selectedNode);
	}
	if (shape != null) {
	    highlightShape(shape);
	}
	selectedNode = shape;
	if (shape != null) {
	    ((Draggable)shape).start(x, y);
	}
        if (shape instanceof MetroStation){
            workspace.getMetroStationsChoiceBox().getSelectionModel().select(shape);
            lastSelectedStation = (MetroStation) shape;
        }
        
        if (shape instanceof DraggableEndText){
            DraggableEndText endText = (DraggableEndText) shape;
            workspace.getMetroLinesChoiceBox().getSelectionModel().select(endText.getLine());
            unhighlightShape(endText.getLine());
            selectedNode = shape; //recast as the set on action overwrites this
            highlightShape(selectedNode);
            lastSelectedLine = endText.getLine();
        }
	return shape;
    }
     
    public Node getTopNode(int x, int y) {
	for (int i = mapNodes.size() - 1; i >= 0; i--) {
	    Node shape = mapNodes.get(i);
            if (shape instanceof Draggable){
                if (shape.contains(x, y)) {
                    return shape;
                }
            }
	}
	return null;
    }
    
    public void highlightShape(Node shape) {
	shape.setEffect(highlightedEffect);   
    }
    
    public void unhighlightShape(Node shape) {
	selectedNode.setEffect(null);
    }
    
    //METHODS THAT EDIT OBJECTS
    public void changeLineThickness(double thicknessValue){
        if(lastSelectedLine != null){
            ShapeEditTransaction transaction = new ShapeEditTransaction(lastSelectedLine, thicknessValue);
            transactionHandler.addTransaction(transaction);
        }
    }
    
    public void changeStationRadius(double radiusValue){
        if (lastSelectedStation != null){
            ShapeEditTransaction transaction = new ShapeEditTransaction(lastSelectedStation, radiusValue);
            transactionHandler.addTransaction(transaction);
        }
    }
    
     public void moveStationLabel() {
         if(lastSelectedStation != null){
            EditStationTransaction transaction = new EditStationTransaction(lastSelectedStation, 
                    EditStationTransaction.MOVE);
            transactionHandler.addTransaction(transaction);
         }
    }
    
    public void rotateStationLabel() {
        if(lastSelectedStation != null){
            EditStationTransaction transaction = new EditStationTransaction(lastSelectedStation, 
                    EditStationTransaction.ROTATE);
            transactionHandler.addTransaction(transaction);
        }
    }
    
     public void undoAction(){
        transactionHandler.undoTransaction();
    }
    
    public void redoAction(){
        transactionHandler.doTransaction();
    }

    
    
     @Override
    //TODO
    public void resetData(){ 
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        setState(SELECTING_NODE);
        transactionHandler.clear();
        selectedNode = null;
        mapNodes.clear();
        lines.clear();
        stations.clear();
        backgroundColor = (Color) DEFAULT_BACKGROUND_COLOR;
        backgroundImage = "null";
        canvas.getChildren().clear();
        canvas.setStyle("-fx-background-color: #" + Integer.toHexString(WHITE_HEX.hashCode()) + ";"
                        + "-fx-background-image: " + "url(" + ");" );
    } 

    

   

    
}
