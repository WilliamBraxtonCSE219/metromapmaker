package M3.data;

import M3.data.Draggable;
import M3.data.M3State;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

/**
 * This is a draggable ellipse for our goLogoLo application.
 * 
 *
 * @author William Braxton
 * @version 1.0
 */
public class MetroStation extends Ellipse implements Draggable, Comparable<MetroStation> {
    private double startCenterX;
    private double startCenterY;
    private double lastX;
    private double lastY;
    private final Text stationName;
    private final List<MetroLine> lines;
    private int labelPos; 
    private int rotate;
    
    public MetroStation(String stationName) {
	setCenterX(30.0);
	setCenterY(30.0);
	setRadiusX(5.0);
	setRadiusY(5.0);
	setOpacity(1.0);
	startCenterX = 0.0;
	startCenterY = 0.0;
        this.stationName = new Text(stationName);
        lines = new ArrayList<>();
        labelPos = 0;
        rotate = 0;
        positionLabel();
    }
    
    
    
    private void positionLabel(){
        switch(labelPos){
            case 0: //0 IS UPPER RIGHT
                stationName.xProperty().bind(this.centerXProperty().add(20));
                stationName.yProperty().bind(this.centerYProperty().subtract(20));
                //stationName.setX(getCenterX() + 20);
                //stationName.setY(getCenterY() - 20);
                break;
            case 1://1 IS LOWER RIGHT
                stationName.xProperty().bind(this.centerXProperty().add(20));
                stationName.yProperty().bind(this.centerYProperty().add(20));
                break;
            case 2://2 IS LOWER LEFT
                stationName.xProperty().bind(this.centerXProperty().subtract(40));
                stationName.yProperty().bind(this.centerYProperty().add(20));
                break;
            case 3://3 IS UPPER LEFT
                stationName.xProperty().bind(this.centerXProperty().subtract(40));
                stationName.yProperty().bind(this.centerYProperty().subtract(20));
                break;
        } 
    }
    
    private void rotateLabelPosition(){
        switch(rotate){
            case 0:
                stationName.setRotate(0);
                break;
            case 1://1 IS LOWER RIGHT
                stationName.setRotate(90);
                break;
            case 2://2 IS LOWER LEFT
                stationName.setRotate(180);
                break;
            case 3://3 IS UPPER LEFT
                stationName.setRotate(270);
                break;
        } 
        
    }
    
    /**
     * returns whether the station is already on a given line
     * @param line
     * @return 
     */
    public Boolean isOnLine (MetroLine line){
        for (MetroLine currentLine : lines){
            if (line.equals(currentLine)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * rotates the label to the next position of the four, then 
     * re[positions it!
     */
    public void moveLabel(){ 
        if (labelPos == 3){
            labelPos = 0;
        }
        else{
            labelPos += 1;
        }
        positionLabel(); //then position it!  
    }
    
     public void rotateLabel(){ 
        if (rotate == 3){
            rotate = 0;
        }
        else{
            rotate += 1;
        }
        rotateLabelPosition(); //then position it!  
    }
    @Override
    public void start(int x, int y) {
	startCenterX = x;
	startCenterY = y;
        lastX = getCenterX();
        lastY = getCenterY();
        
    }
    
    public void snap(){
        int x =  (int) Math.round(getX()/20);
        int y =  (int) Math.round(getY()/20);
        setX(x * 20);
        setY(y * 20);
        
    }
    /**
     * when this is going to be removed, we must remove it from all
     * lines it is apart of
     */
    public void processRemoval(){
        for(MetroLine line : lines){
            line.removeStationOnly(this);
        }
    }
    
    public void addMetroLine(MetroLine line){
        this.lines.add(line);
    }
    public void removeMetroLine(MetroLine line){
        this.lines.remove(line);
    }
    
    public Text getText(){
        return stationName;
    }
    
    public int getRotation(){
        return rotate;
    }
    
    public List getLineList(){
        return lines;
    }
    
    /**
     *Values 0 through 3. If it does not fit these numbers, we do nothing
     * @param rotate
     * 
     */
    public void setRotation(int rotate){
        if(rotate < 4 && rotate >= 0){
            this.rotate = rotate;
            rotateLabelPosition();
        }
    }
    
    
    public int getLabelPosition(){
        return labelPos;
    }
    
    /**
     *Values 0 through 3. If it does not fit these numbers, we do nothing
     * @param labelPos
     * 
     */
    public void setLabelPosition(int labelPos){
        if(labelPos < 4 && labelPos >= 0){
            this.labelPos = labelPos;
            positionLabel();
        }
    }
    
    public String getStationName(){
        return stationName.getText();
    }
    public void setText(String text){
        stationName.setText(text);
    }
    
    @Override
    public String toString(){
        return stationName.getText();
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x -  (startCenterX );
	double diffY = y -  (startCenterY );
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startCenterX = x;
	startCenterY = y;
    }
    
   
    @Override
    public void size(int x, int y) {
	double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadiusX(width / 2);
	setRadiusY(height / 2);	
	
    }
        
    @Override
    public double getX() {
	//return getCenterX() - getRadiusX();
	return getCenterX();
    }

    @Override
    public double getY() {
	//return getCenterY() - getRadiusY();
	return getCenterY();
    }
    
    @Override
    public void setX(int x) {
        setCenterX(x);
    }

    public void setRadius(double radius){
        setRadiusX(radius);
        setRadiusY(radius);
    }
    @Override
    public void setY(int y) {
	setCenterY(y);
    }
    
    
    @Override
    public double getLastX() {
	return lastX;
    }

    @Override
    public double getLastY() {
	return lastY;
    }

    @Override
    public double getWidth() {
	return getRadiusX() * 2;
    }

    @Override
    public double getHeight() {
	return getRadiusY() * 2;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadiusX(initWidth/2);
	setRadiusY(initHeight/2);
    }
    
    @Override
    public String getShapeType() {
	return STATION;
    }
    
     @Override
    public boolean equals(Object other) {
        if(other == null)
            return false;
        if(other == this)
            return true;
        if(other.getClass() != getClass())
            return false;
        MetroStation ow = (MetroStation)other;
        return stationName.getText().equals(ow.getStationName());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.stationName.getText());
        return hash;
    }
    
     @Override
    public Draggable clone(){
        MetroStation cloneStation = new MetroStation(stationName.getText());
        cloneStation.setFill(getFill());
        cloneStation.setStroke(getStroke());
        cloneStation.setCenterX(getCenterX());
	cloneStation.setCenterY(getCenterY());
	cloneStation.setRadiusX(getRadiusX());
	cloneStation.setRadiusY(getRadiusX());
        return cloneStation;
    }



    @Override
    public int compareTo(MetroStation station) {
        return stationName.getText().compareTo(station.getStationName());   
    }
}
