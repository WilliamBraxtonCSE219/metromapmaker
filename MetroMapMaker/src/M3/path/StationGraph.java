/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.path;

import M3.data.MetroLine;
import M3.data.MetroStation;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 *
 * @author William
 */
public class StationGraph extends AbstractEdgeWeightedGraph<MetroStation> {
    
    private final HashSet<MetroStation> stationSet;
    
    private final HashMap<MetroStation, Set<MetroStation>> stationMap;
    
    
    public StationGraph(ObservableList<Node> listOfLines){
        stationSet = new HashSet<>();
        stationMap = new HashMap<>();
        List<MetroStation> listOfStations;
        for(Node node : listOfLines){
           MetroLine currentLine = (MetroLine) node;
           listOfStations = currentLine.getStationList();
           for(MetroStation station : listOfStations){
               if(!stationSet.contains(station)){
                stationSet.add(station);
               }
            }
        }
        for(Node node : listOfLines){
           MetroLine currentLine = (MetroLine) node;
           listOfStations = currentLine.getStationList();
           if(currentLine.isCircular()){
               addToMap(listOfStations.get(listOfStations.size() - 1), listOfStations.get(0));
               addToMap(listOfStations.get(0), listOfStations.get(listOfStations.size() - 1));
           } 
           for(int i = 0; i < listOfStations.size() - 1; i++){
               MetroStation stationFrom = listOfStations.get(i);
               MetroStation stationTo = listOfStations.get(i+1);
               addToMap(stationFrom, stationTo);
               addToMap(stationTo, stationFrom);
           }
        }
    }
    
     /**
     * Insert a pair of words in the map.
     * 
     * @param from The word to be used as the key.
     * @param to The word to be added to the list of words associated
     * with the key.
     */
    private void addToMap(MetroStation from, MetroStation to) {
        Set<MetroStation> s = stationMap.get(from);
        if(s == null) {
            s = new HashSet<>();
            stationMap.put(from, s);
        }
        if(!s.contains(to)){
            s.add(to);
        }
    }
    
    @Override
    public boolean containsNode(MetroStation station) {
        return stationSet.contains(station);
    }
    
    @Override
    public boolean isAdjacent(MetroStation from, MetroStation to) {
        Set<MetroStation> s = stationMap.get(from);
        if(s == null)
            return false;
        else
            return s.contains(to);
    }
    
     @Override
    public Iterator<MetroStation> nodeIterator(MetroStation from) {
        if(from == null)
            return(stationSet.iterator());
        else {
            Set<MetroStation> l = stationMap.get(from);
            if(l == null)
                l = new HashSet<>();
            return l.iterator();
        }
    }
    
    @Override
    public double edgeWeight(MetroStation from, MetroStation to) {
        if(isAdjacent(from, to))
            return 1;
        else
            return Double.POSITIVE_INFINITY;
    }
}
   

