/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import static M3.M3LangaugeProperty.IMAGE_LOAD_ERROR;
import static M3.M3LangaugeProperty.IMAGE_LOAD_MESSAGE;
import static M3.M3LangaugeProperty.IMAGE_TITLE;
import M3.data.DraggableImage;
import M3.data.M3Data;
import djf.AppTemplate;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.util.Optional;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;

/**
 *
 * @author William
 */
public class NodeCreateController {
    AppTemplate app;
    M3Data dataManager;
    AppGUI gui;
    
    public NodeCreateController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();
    }
    
    public void processAddMetroLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        
        dataManager.addLine();
        
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveMetroLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Remove Line");
        alert.setContentText("Are you sure you want to remove this line?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
        dataManager.removeLine();
        }
        
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
	workspace.reloadWorkspace(dataManager);
    }

    public void processAddStation() {
        // CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
        
        dataManager.addStation();
        
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveStation() {
        Scene scene = app.getGUI().getPrimaryScene();
        
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Remove Station");
        alert.setContentText("Are you sure you want to remove this station??");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            dataManager.removeStation(); 
        }
        
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

     public void processAddImage() {
	// CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	//scene.setCursor(Cursor.CROSSHAIR);
        
	//SET THE FILECHOOSER AND ITS FILTER
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg");
	FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_IMAGES));
	fc.setTitle(app.getProperty(IMAGE_TITLE));
        fc.setSelectedExtensionFilter(imageFilter);
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        if (selectedFile != null) {
            try{
                //LOAD THE IMAGE
               DraggableImage image = new DraggableImage("File:" + selectedFile.getPath());
               //ADD THE IMAGE TO THE CANVAS
               dataManager.addImage(image);
               
               app.getGUI().updateToolbarControls(false);
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(app.getProperty(IMAGE_LOAD_ERROR), app.getProperty(IMAGE_LOAD_MESSAGE));
            }
	// ENABLE/DISABLE THE PROPER BUTTONS
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    }

    public void processaddLabel() {
        Scene scene = app.getGUI().getPrimaryScene();
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.addText();
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
        workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveElement() {
        // REMOVE THE SELECTED SHAPE IF THERE IS ONE
	dataManager.removeNode();
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }
    
}
