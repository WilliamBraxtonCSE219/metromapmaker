/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import static M3.M3LangaugeProperty.ABOUT_ICON;
import static M3.M3LangaugeProperty.ABOUT_TOOLTIP;
import static M3.M3LangaugeProperty.ADD_IMAGE_TEXT;
import static M3.M3LangaugeProperty.ADD_IMAGE_TOOLTIP;
import static M3.M3LangaugeProperty.ADD_LABEL_TEXT;
import static M3.M3LangaugeProperty.ADD_LABEL_TOOLTIP;
import static M3.M3LangaugeProperty.ADD_LINE_TOOLTIP;
import static M3.M3LangaugeProperty.ADD_STATION_TOOLTIP;
import static M3.M3LangaugeProperty.ADD_STATION_TO_LINE_TEXT;
import static M3.M3LangaugeProperty.ADD_STATION_TO_LINE_TOOLTIP;
import static M3.M3LangaugeProperty.BOLD_ICON;
import static M3.M3LangaugeProperty.BOLD_TOOLTIP;
import static M3.M3LangaugeProperty.COPY_ICON;
import static M3.M3LangaugeProperty.COPY_TOOLTIP;
import static M3.M3LangaugeProperty.CUT_ICON;
import static M3.M3LangaugeProperty.CUT_TOOLTIP;
import static M3.M3LangaugeProperty.DECOR_LABEL;
import static M3.M3LangaugeProperty.DECREASE_SIZE_ICON;
import static M3.M3LangaugeProperty.DECREASE_SIZE_TOOLTIP;
import static M3.M3LangaugeProperty.EDIT_LINE_TEXT;
import static M3.M3LangaugeProperty.EDIT_LINE_TOOLTIP;
import static M3.M3LangaugeProperty.EDIT_STATION_TEXT;
import static M3.M3LangaugeProperty.END_STATION_TOOLTIP;
import static M3.M3LangaugeProperty.FIND_ROUTE_ICON;
import static M3.M3LangaugeProperty.FIND_ROUTE_TOOLTIP;
import static M3.M3LangaugeProperty.FONT_LABEL;
import static M3.M3LangaugeProperty.GRID_TOGGLE_LABEL;
import static M3.M3LangaugeProperty.GRID_TOGGLE_TOOLTIP;
import static M3.M3LangaugeProperty.IMAGE_BACKGROUND_TEXT;
import static M3.M3LangaugeProperty.IMAGE_BACKGROUND_TOOLTIP;
import static M3.M3LangaugeProperty.INCREASE_SIZE_ICON;
import static M3.M3LangaugeProperty.INCREASE_SIZE_TOOLTIP;
import static M3.M3LangaugeProperty.ITALICS_ICON;
import static M3.M3LangaugeProperty.ITALICS_TOOLTIP;
import static M3.M3LangaugeProperty.LIST_STATIONS_ICON;
import static M3.M3LangaugeProperty.LIST_STATIONS_TOOLTIP;
import static M3.M3LangaugeProperty.METRO_LINES_DROPLIST_TOOLTIP;
import static M3.M3LangaugeProperty.METRO_LINES_LABEL;
import static M3.M3LangaugeProperty.METRO_STATIONS_DROPLIST_TOOLTIP;
import static M3.M3LangaugeProperty.METRO_STATIONS_LABEL;
import static M3.M3LangaugeProperty.MINUS_BUTTON_ICON;
import static M3.M3LangaugeProperty.MOVE_STATION_LABEL_TEXT;
import static M3.M3LangaugeProperty.MOVE_STATION_LABEL_TOOLTIP;
import static M3.M3LangaugeProperty.NAVIGATION_LABEL;
import static M3.M3LangaugeProperty.PASTE_ICON;
import static M3.M3LangaugeProperty.PASTE_TOOLTIP;
import static M3.M3LangaugeProperty.PLUS_BUTTON_ICON;
import static M3.M3LangaugeProperty.REDO_ICON;
import static M3.M3LangaugeProperty.REDO_TOOLTIP;
import static M3.M3LangaugeProperty.REMOVE_ELEMENT_TEXT;
import static M3.M3LangaugeProperty.REMOVE_ELEMENT_TOOLTIP;
import static M3.M3LangaugeProperty.REMOVE_LINE_TOOLTIP;
import static M3.M3LangaugeProperty.REMOVE_STATION_FROM_LINE_TEXT;
import static M3.M3LangaugeProperty.REMOVE_STATION_FROM_LINE_TOOLTIP;
import static M3.M3LangaugeProperty.REMOVE_STATION_TOOLTIP;
import static M3.M3LangaugeProperty.ROTATE_STATION_LABEL_TEXT;
import static M3.M3LangaugeProperty.ROTATE_STATION_LABEL_TOOLTIP;
import static M3.M3LangaugeProperty.SNAP_TO_GRID_TEXT;
import static M3.M3LangaugeProperty.SNAP_TO_GRID_TOOLTIP;
import static M3.M3LangaugeProperty.START_STATION_TOOLTIP;
import static M3.M3LangaugeProperty.TEXT_FAMILY_TOOLTIP;
import static M3.M3LangaugeProperty.TEXT_SIZE_TOOLTIP;
import static M3.M3LangaugeProperty.UNDO_ICON;
import static M3.M3LangaugeProperty.UNDO_TOOLTIP;
import static M3.M3LangaugeProperty.ZOOM_IN_ICON;
import static M3.M3LangaugeProperty.ZOOM_IN_TOOLTIP;
import static M3.M3LangaugeProperty.ZOOM_OUT_ICON;
import static M3.M3LangaugeProperty.ZOOM_OUT_TOOLTIP;
import static M3.css.M3Style.CLASS_EDIT_TOOLBAR;
import static M3.css.M3Style.CLASS_EDIT_TOOLBAR_ROW;
import static M3.css.M3Style.CLASS_RENDER_CANVAS;
import M3.data.DraggableImage;
import M3.data.DraggableText;
import M3.data.M3Data;
import static M3.data.M3Data.BLACK_HEX;
import M3.data.MetroLine;
import M3.data.MetroStation;
import M3.metroMapMakerApp;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.ui.AppGUI;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import static djf.ui.AppGUI.CLASS_FILE_BUTTON;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import jtps.jTPS;

/**
 *
 * @author William Braxton
 */
public class M3Workspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    //Toolbar for editing tools
    ScrollPane editToolbarScroll;
    VBox editToolbar;
    
    //INITIALIZE BUTTONS TO BE ADDED TOTOP TOOL BAR
    FlowPane settingsToolbar;
    Button aboutButton;
    
    FlowPane clipboardToolbar;
    Button cutButton;
    Button copyButton;
    Button pasteButton;
    
    FlowPane transactionToolbar;
    Button undoButton;
    Button redoButton;
    
    //METRO LINES SECTION
    VBox metroLineSection; //hold each row for metrolinesection
    
    //row 1
    StackPane metroLineDropList;
    
    HBox metroLinesLabelCell;
    Label metroLinesLabel;
    ChoiceBox metroLinesDropMenu;
    ObservableList<Node> metroLines;
    
    HBox metroLinesColorPickerCell;
    Button metroLinesEditLineBtn;
    
    //row 2
    HBox metroLineButtons; 
    Button addMetroLineButton;
    Button removeMetroLineButton;
    Button addStationToLineButton;
    Button removeStationFromLineButton;
    Button listStationsButton;
    
    //row 3
    HBox metroLinesSliderRow; //holds drop list as well as color chooser
    Slider metroLinesSlider;
    
   //METRO STATIONS SECTION
    VBox metroStationSection; //holds each row for metro station section
    
    //row 1
    StackPane metroStationDropList; //holds drop list as well as color chooser
    
    HBox metroStationsLabelCell;
    Label metroStationsLabel;
    ChoiceBox metroStationsDropMenu;
    ObservableList<Node> stations;
    
    HBox metroStationsColorPickerCell;
    ColorPicker metroStationsColorPicker;
 
    //row 2
    HBox metroStationsButtons; 
    Button addMetroStationsButton;
    Button removeMetroStationsButton;
    Button snapStationToGridButton;
    Button moveStationLabelButton;
    Button rotateStationLabelButton;
    
     //row 3
    HBox metroStationsSliderRow; 
    Slider metroStationsSlider;
    
    //FIND ROUTE SECTION
    HBox findRouteSection;
    
    //column 1
    VBox stationToRoute;
    ChoiceBox startStationDropMenu;
    ChoiceBox endStationDropMenu;
    
    //column 2
    HBox findRouteBox;
    Button findrouteButton;
    
    //DECOR SECTION
    VBox decorSection;
    
    //row 1
    StackPane labelAndColorRow;
    StackPane decorLabelCell;
    StackPane decorColorPickerCell;
    Label decorLabel;
    ColorPicker BackgroundColorPicker;
    
    //row 2
    HBox DecorButtons;
    Button setImageBackgroundButton;
    Button addImageButton;
    Button addLabelButton;
    Button removeElementButton;
    
    //FONT SECTION
    VBox fontSection;
    
    //row 1
    StackPane fontLabelRow;
    StackPane fontLabelCell;
    StackPane fontColorPickerCell;
    Label fontLabel;
    ColorPicker fontColorPicker;
    
    //row 2
    HBox fontButtonsRow;
    ToggleButton boldButton;
    ToggleButton italicsButton;
    ComboBox fontSizeComboBox;
    ComboBox fontFamilyComboBox;
    ObservableList<String> families;
    ObservableList<String> fontSizeList;
    
    //NAVIGATION SECTION
    VBox navigationSection;
    
    //row 1
    StackPane navigationLabelRow;
    HBox navigationLabelCell;
    HBox navigationToggleCell;
    
    Label navigationLabel;
    Label gridToggleLabel;
    CheckBox gridToggle;
    
    //row 2
    HBox naviagationButtons;
    Button zoomInButton;
    Button zoomOutButton;
    Button increaseMapSizeButton;
    Button decreaseMapSizeButton;
    
    //THIS IS WHERE WE WILL HAVE OUR CANVAS THAT
    //IS REALLY A PANE
    Pane canvas;
    Group grid;
    
    KeyEvent W;
    KeyEvent A;
    KeyEvent S;
    KeyEvent D;
    
     // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    MapEditController mapEditController;
    MapAdjustmentController mapAdjustmentController;
    NodeCreateController nodeCreateController;
    NodeEditController nodeEditController;
    TransactionController transactionController;
    ColorController colorController;
    SelectionController selectionController;
    

    // HERE ARE OUR DIALOGS
   
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    public M3Workspace(AppTemplate initApp) {
       
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();    

    }

   
     
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
   public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
     
    public Slider getMetroLinesSlider() {
	return metroLinesSlider;
    }
    
    public Slider getMetroStationsSlider() {
	return metroStationsSlider;
    }
    
    public ColorPicker getMetroStationsColorPicker() {
	return metroStationsColorPicker;
    }
    
    public ColorPicker getBackgroundColorPicker() {
	return BackgroundColorPicker;
    }
    
    public ColorPicker getFontColorPicker() {
	return fontColorPicker;
    }
    
    public ChoiceBox getMetroLinesChoiceBox(){
        return metroLinesDropMenu;
    }
    
    public ChoiceBox getMetroStationsChoiceBox(){
        return metroStationsDropMenu;
    }

    public Pane getCanvas() {
	return canvas;
    }
    
    //THIS WILL INITIALIZE EVERYTHING IN OUR GUI
    private void initLayout() {
    // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
    canvas = new Pane();
    debugText = new Text();
    canvas.getChildren().add(debugText);
    debugText.setX(100);
    debugText.setY(100);

    M3Data data = (M3Data)app.getDataComponent();
    // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
    data.setShapes(canvas.getChildren());
    //ADD OUR CUT COPY PASTE AND OTHER TOPTOOLBAR STUFF
    settingsToolbar = new FlowPane();
    aboutButton = gui.initChildButton(settingsToolbar, ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        
    //ADD TO ANOTHER TOOLBAR AT THE TOP
    clipboardToolbar = new FlowPane();
    cutButton = gui.initChildButton(clipboardToolbar, CUT_ICON.toString(), CUT_TOOLTIP.toString(), true);
    copyButton = gui.initChildButton(clipboardToolbar, COPY_ICON.toString(), COPY_TOOLTIP.toString(), true);
    pasteButton = gui.initChildButton(clipboardToolbar, PASTE_ICON.toString(), PASTE_TOOLTIP.toString(), true);
        
    //ADD ANOTHER TOOLBAR AT THE TOP FOR TRANSACTIONS
    transactionToolbar = new FlowPane();
    undoButton = gui.initChildButton(transactionToolbar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
    redoButton = gui.initChildButton(transactionToolbar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);

    // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
    editToolbarScroll = new ScrollPane();
    editToolbar = new VBox();
    editToolbarScroll.setContent(editToolbar);
    //METROLINESSECTION
    metroLineSection = new VBox(); //hold each row for metrolinesection
 
    //row 1
    metroLineDropList = new StackPane(); //holds drop list as well as color chooser
    metroLineDropList.setAlignment(Pos.CENTER);
    metroLinesLabelCell = new HBox(5);
    metroLinesLabelCell.setAlignment(Pos.CENTER_LEFT);
    metroLineDropList.getChildren().add(metroLinesLabelCell);
    metroLinesColorPickerCell = new HBox();
    metroLinesColorPickerCell.setAlignment(Pos.CENTER_RIGHT);
    //metroLineDropList.getChildren().add(metroLinesColorPickerCell);
    HBox.setHgrow(metroLinesColorPickerCell, Priority.ALWAYS);

    
    metroLines = data.getLines();
    metroLinesLabel = gui.initLabel(metroLinesLabelCell, METRO_LINES_LABEL.toString());
    metroLinesDropMenu = gui.initChoiceBox(metroLinesLabelCell, metroLines, METRO_LINES_DROPLIST_TOOLTIP.toString(), false);
    metroLinesEditLineBtn = gui.initChildButtonText(metroLinesColorPickerCell, EDIT_LINE_TEXT.toString(), EDIT_LINE_TOOLTIP.toString(), false);
    metroLinesLabelCell.getChildren().add(metroLinesColorPickerCell);
    HBox.setHgrow(metroLinesColorPickerCell, Priority.ALWAYS);
    
    //row 2
    metroLineButtons = new HBox(5); 
    addMetroLineButton =  gui.initChildButton(metroLineButtons, PLUS_BUTTON_ICON.toString(), ADD_LINE_TOOLTIP.toString(), false);
    removeMetroLineButton = gui.initChildButton(metroLineButtons, MINUS_BUTTON_ICON.toString(), REMOVE_LINE_TOOLTIP.toString(), true);
    addStationToLineButton = gui.initChildButtonText(metroLineButtons, ADD_STATION_TO_LINE_TEXT.toString(), ADD_STATION_TO_LINE_TOOLTIP.toString(), true);
    removeStationFromLineButton= gui.initChildButtonText(metroLineButtons, REMOVE_STATION_FROM_LINE_TEXT.toString(), REMOVE_STATION_FROM_LINE_TOOLTIP.toString(), true);
    listStationsButton = gui.initChildButton(metroLineButtons, LIST_STATIONS_ICON.toString(), LIST_STATIONS_TOOLTIP.toString(), true);
    
    //row 3
    metroLinesSliderRow = new HBox(); //holds drop list as well as color chooser
    metroLinesSliderRow.setAlignment(Pos.CENTER);
    metroLinesSlider = new Slider(1, 5, 1);
    metroLinesSlider.setDisable(true);
    metroLinesSliderRow.getChildren().add(metroLinesSlider);
    
   //METRO STATIONS SECTION
    metroStationSection = new VBox(); //holds each row for metro station section
    
    //row 1
    metroStationDropList = new StackPane(); //holds drop list as well as color chooser
    metroStationDropList.setAlignment(Pos.CENTER);
    metroStationsLabelCell = new HBox(5);
    metroStationsLabelCell.setAlignment(Pos.CENTER_LEFT);
    metroStationDropList.getChildren().add(metroStationsLabelCell);
    metroStationsColorPickerCell = new HBox();
    metroStationsColorPickerCell.setAlignment(Pos.CENTER_RIGHT);
    //metroStationDropList.getChildren().add(metroStationsColorPickerCell);
    stations = data.getStations();
    metroStationsLabel = gui.initLabel(metroStationsLabelCell, METRO_STATIONS_LABEL.toString());
    metroStationsDropMenu = gui.initChoiceBox(metroStationsLabelCell, stations, METRO_STATIONS_DROPLIST_TOOLTIP.toString(), false);
    metroStationsColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
    metroStationsColorPickerCell.getChildren().add(metroStationsColorPicker);
    metroStationsLabelCell.getChildren().add(metroStationsColorPickerCell);
    HBox.setHgrow(metroStationsColorPickerCell, Priority.ALWAYS);

    //row 2
    metroStationsButtons = new HBox(5);
    addMetroStationsButton=  gui.initChildButton(metroStationsButtons, PLUS_BUTTON_ICON.toString(), ADD_STATION_TOOLTIP.toString(), false);
    removeMetroStationsButton = gui.initChildButton(metroStationsButtons, MINUS_BUTTON_ICON.toString(), REMOVE_STATION_TOOLTIP.toString(), true);
    snapStationToGridButton = gui.initChildButtonText(metroStationsButtons, SNAP_TO_GRID_TEXT.toString(), SNAP_TO_GRID_TOOLTIP.toString(), true);
    moveStationLabelButton = gui.initChildButtonText(metroStationsButtons, MOVE_STATION_LABEL_TEXT.toString(), MOVE_STATION_LABEL_TOOLTIP.toString(), true);
    rotateStationLabelButton = gui.initChildButtonText(metroStationsButtons, ROTATE_STATION_LABEL_TEXT.toString(), ROTATE_STATION_LABEL_TOOLTIP.toString(), true);
    
     //row 3
    metroStationsSliderRow = new HBox(); //holds drop list as well as color chooser
    metroStationsSliderRow.setAlignment(Pos.CENTER);
    metroStationsSlider = new Slider(5, 10, 5);
    metroStationsSlider.setDisable(true);
    metroStationsSliderRow.getChildren().add(metroStationsSlider);

    //FIND ROUTE SECTION
    findRouteSection = new HBox();
   
    
    //column 1
    stationToRoute = new VBox(4);
    startStationDropMenu = gui.initChoiceBox(stationToRoute, stations, START_STATION_TOOLTIP.toString(), false);
    endStationDropMenu = gui.initChoiceBox(stationToRoute, stations, END_STATION_TOOLTIP.toString(), false);
    
    //column 2
    findRouteBox = new HBox();
    findRouteBox.setAlignment(Pos.CENTER);
    findrouteButton = gui.initChildButton(findRouteBox, FIND_ROUTE_ICON.toString(), FIND_ROUTE_TOOLTIP.toString(), workspaceActivated);
    
    //DECOR SECTION
    decorSection = new VBox();
    
    //row 1
    labelAndColorRow = new StackPane();
    labelAndColorRow.setAlignment(Pos.CENTER);
    decorLabelCell = new StackPane();
    decorLabelCell.setAlignment(Pos.CENTER_LEFT);
    labelAndColorRow.getChildren().add(decorLabelCell);
    decorColorPickerCell = new StackPane();
    labelAndColorRow.getChildren().add(decorColorPickerCell);
    decorColorPickerCell.setAlignment(Pos.CENTER_RIGHT);
    decorLabel = gui.initLabel(decorLabelCell, DECOR_LABEL.toString());
    BackgroundColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
    decorColorPickerCell.getChildren().add(BackgroundColorPicker);
    
    //row 2
    DecorButtons = new HBox(5);
    setImageBackgroundButton = gui.initChildButtonText(DecorButtons, IMAGE_BACKGROUND_TEXT.toString(), IMAGE_BACKGROUND_TOOLTIP.toString(), false);
    addImageButton = gui.initChildButtonText(DecorButtons, ADD_IMAGE_TEXT.toString(), ADD_IMAGE_TOOLTIP.toString(), false);
    addLabelButton = gui.initChildButtonText(DecorButtons, ADD_LABEL_TEXT.toString(), ADD_LABEL_TOOLTIP.toString(), false);
    removeElementButton = gui.initChildButtonText(DecorButtons, REMOVE_ELEMENT_TEXT.toString(), REMOVE_ELEMENT_TOOLTIP.toString(), true);
    
    //FONT SECTION
    fontSection = new VBox();
    
    //row 1
    fontLabelRow = new StackPane();
    fontLabelRow.setAlignment(Pos.CENTER);
    fontLabelCell = new StackPane();
    fontLabelCell.setAlignment(Pos.CENTER_LEFT);
    fontLabelRow.getChildren().add(fontLabelCell);
    fontColorPickerCell = new StackPane();
    fontColorPickerCell.setAlignment(Pos.CENTER_RIGHT);
    fontLabelRow.getChildren().add(fontColorPickerCell);
    fontLabel = gui.initLabel(fontLabelCell, FONT_LABEL.toString());
    fontColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
    fontColorPickerCell.getChildren().add(fontColorPicker);
    //NOTE!! add color picker for this and everything else
    
    //row 2
    families = FXCollections.observableArrayList(
        "Times New Roman",
        "Verdana",
        "Arial",
        "Comic Sans",
        "Trebuchet MS"
    ); //Text families
    fontSizeList = FXCollections.observableArrayList(
        "8","10","11","12","14","18","20","22","24","26","28","30","32","34","36","38","40","42","44","46"
    ); //Text families
    fontButtonsRow = new HBox(5);
    boldButton = gui.initChildToggleButton(fontButtonsRow, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), true);
    italicsButton =  gui.initChildToggleButton(fontButtonsRow, ITALICS_ICON.toString(), ITALICS_TOOLTIP.toString(), true);
    fontFamilyComboBox = gui.initComboBox(fontButtonsRow, families, TEXT_FAMILY_TOOLTIP.toString(), false);
    fontSizeComboBox = gui.initComboBox(fontButtonsRow, fontSizeList, TEXT_SIZE_TOOLTIP.toString(), false);
    
    
    //NAVIGATION SECTION
    navigationSection = new VBox();
    
    //row 1
    navigationLabelRow = new StackPane();
    navigationLabelCell = new HBox();
    navigationLabelCell.setAlignment(Pos.TOP_LEFT);
    navigationToggleCell = new HBox(2);
    navigationToggleCell.setAlignment(Pos.TOP_RIGHT);
    
    navigationLabel = gui.initLabel(navigationLabelCell, NAVIGATION_LABEL.toString());
    gridToggleLabel = gui.initLabel(navigationToggleCell, GRID_TOGGLE_LABEL.toString());
    gridToggle = gui.initCheckBox(navigationToggleCell, GRID_TOGGLE_TOOLTIP.toString());
    //row 2
    naviagationButtons = new HBox(5);
    zoomInButton = gui.initChildButton(naviagationButtons, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
    zoomOutButton = gui.initChildButton(naviagationButtons, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
    increaseMapSizeButton = gui.initChildButton(naviagationButtons, INCREASE_SIZE_ICON.toString(), INCREASE_SIZE_TOOLTIP.toString(), false);
    decreaseMapSizeButton = gui.initChildButton(naviagationButtons, DECREASE_SIZE_ICON.toString(), DECREASE_SIZE_TOOLTIP.toString(), false);
    
    //PUT EVERYTHING IN ITS RIGHTFUL SECTION1
    metroLineSection.getChildren().add(metroLineDropList);
    metroLineSection.getChildren().add(metroLineButtons);
    metroLineSection.getChildren().add(metroLinesSliderRow);
    
    metroStationSection.getChildren().add(metroStationDropList);
    metroStationSection.getChildren().add(metroStationsButtons);
    metroStationSection.getChildren().add(metroStationsSliderRow);

    findRouteSection.getChildren().add(stationToRoute);
    findRouteSection.getChildren().add(findRouteBox);

    decorSection.getChildren().add(labelAndColorRow);
    decorSection.getChildren().add(DecorButtons);
    
    fontSection.getChildren().add(fontLabelRow);
    fontSection.getChildren().add(fontButtonsRow);
    
    navigationSection.getChildren().add(navigationLabelRow);
    navigationLabelRow.getChildren().add(navigationLabelCell);
    navigationLabelRow.getChildren().add(navigationToggleCell);
    navigationSection.getChildren().add(naviagationButtons);
    
   
    //ORGANIZE THE EDIT TOOLBAR
    editToolbar.getChildren().add(metroLineSection);
    editToolbar.getChildren().add(metroStationSection);
    editToolbar.getChildren().add(findRouteSection);
    editToolbar.getChildren().add(decorSection);
    editToolbar.getChildren().add(fontSection);
    editToolbar.getChildren().add(navigationSection);
    
    // AND NOW SETUP THE WORKSPACE
    workspace = new BorderPane();
    ((BorderPane)workspace).setCenter(canvas);
    ((BorderPane)workspace).setLeft(editToolbarScroll);
    
    //ADD NEW TOP TOOL BAR
    gui.getTopToolbarPane().getChildren().add(clipboardToolbar);
    gui.getTopToolbarPane().getChildren().add(transactionToolbar);
    gui.getTopToolbarPane().getChildren().add(settingsToolbar);
    
    }
    
    //THIS WILL INITIALIZE ALL CONTROL HANDLERS
    private void initControllers() {
        mapEditController = new MapEditController(app);
        mapAdjustmentController = new MapAdjustmentController(app);
        nodeCreateController = new NodeCreateController(app);
        nodeEditController = new NodeEditController(app);
        transactionController = new TransactionController(app);
        colorController = new ColorController(app);
        selectionController = new SelectionController(app);
        
        //ADD FUNCTION TO TOOLBAR BUTTONS
        aboutButton.setOnAction(e->{
            mapEditController.showAboutScreen();
        });
        
        undoButton.setOnAction(e->{
            transactionController.processUndo();
        });
        redoButton.setOnAction(e->{
            transactionController.processRedo();
        });
        
        //CONNECT BUTTONS TO HANDLERS
        //FIRST DROP MENUS
        metroLinesDropMenu.setOnAction(e-> {
            selectionController.processSelectMetroLine(metroLinesDropMenu.getSelectionModel().getSelectedItem());
        });
        
        metroStationsDropMenu.setOnAction(e-> {
            selectionController.processSelectStation(metroStationsDropMenu.getSelectionModel().getSelectedItem());
        });
        
        fontSizeComboBox.setOnAction(e-> {
            nodeEditController.processChangeFontSize((String) fontSizeComboBox.getValue());
        });
        
        fontFamilyComboBox.setOnAction(e-> {
            nodeEditController.processChangeFontFamily((String) fontFamilyComboBox.getValue());
        });
        
        startStationDropMenu.setOnAction(e->{
            reloadWorkspace(app.getDataComponent());
        });
        
        endStationDropMenu.setOnAction(e->{
            reloadWorkspace(app.getDataComponent());
        });
        //NOW COLOR PICKERS!
        
        
        metroStationsColorPicker.setOnAction(e-> {
            colorController.processChangeStationColor();
        });
        
        BackgroundColorPicker.setOnAction(e-> {
            mapEditController.processChangeBackgroundColor();
        });
        
        fontColorPicker.setOnAction(e-> {
            colorController.processChangeFontColor();
        });
        
        //NOW FOR BUTTONS!
        addMetroLineButton.setOnAction(e-> {
            nodeCreateController.processAddMetroLine();
        });
        
        metroLinesEditLineBtn.setOnAction(e-> {
            nodeEditController.processEditLine();
        });
        removeMetroLineButton.setOnAction(e-> {
            nodeCreateController.processRemoveMetroLine();
        });
        
        addStationToLineButton.setOnAction(e-> {
            nodeEditController.processAddStationToLine();
        });
        
        removeStationFromLineButton.setOnAction(e-> {
            nodeEditController.processRemoveStationFromLine();
        });
        
        listStationsButton.setOnAction(e-> {
            mapEditController.processListStations();
        });
        
        addMetroStationsButton.setOnAction(e-> {
            nodeCreateController.processAddStation();
        });
        
        removeMetroStationsButton.setOnAction(e-> {
            nodeCreateController.processRemoveStation();
        });
        
        snapStationToGridButton.setOnAction(e-> {
            mapEditController.processSnapToGrid();
        });
        
        moveStationLabelButton.setOnAction(e-> {
            nodeEditController.processMoveStationLabel();
        });
        
        rotateStationLabelButton.setOnAction(e-> {
            nodeEditController.processRotateStationLabel();
        });
        
        findrouteButton.setOnAction(e-> {
            mapEditController.processFindRoute(startStationDropMenu.getValue(),endStationDropMenu.getValue());
        });
        
        setImageBackgroundButton.setOnAction(e-> {
            mapEditController.processSetImageBackground();
        });
        
        addImageButton.setOnAction(e-> {
            nodeCreateController.processAddImage();
        });
        
        addLabelButton.setOnAction(e-> {
            nodeCreateController.processaddLabel();
        });
        
        removeElementButton.setOnAction((ActionEvent e)-> {
            nodeCreateController.processRemoveElement();
        });
        
        boldButton.setOnAction(e-> {
            nodeEditController.processBoldText();
        });
        
        italicsButton.setOnAction(e-> {
            nodeEditController.processItalicsText();
        });
        
        zoomInButton.setOnAction(e-> {
            mapAdjustmentController.processZoomIn();
        });
        
        zoomOutButton.setOnAction(e-> {
            mapAdjustmentController.processZoomOut();
        });
        
        increaseMapSizeButton.setOnAction(e-> {
            mapAdjustmentController.processIncreaseMapSize();
        });
        
        decreaseMapSizeButton.setOnAction(e-> {
            mapAdjustmentController.processDecreaseMapSize();
        });
        
        gridToggle.setOnAction(e-> {
            mapAdjustmentController.processToggleGrid(gridToggle.selectedProperty().get());
        });
        //LETS DO THE SLIDERS
        metroLinesSlider.valueProperty().addListener(e-> {
            nodeEditController.processChangeLineThickness(metroLinesSlider.getValue());
        });
        
        metroStationsSlider.valueProperty().addListener(e-> {
            nodeEditController.processChangeStationRadius(metroStationsSlider.getValue());
        });
           
        // MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{    
                    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	}); 
        
        canvas.setFocusTraversable(true);
       
        canvas.widthProperty().addListener(e->{
            ((M3Data) app.getDataComponent()).makeGrid();
        });
        gui.getPrimaryScene().setOnKeyPressed(e ->{
            switch(e.getCode()){
                case W:
                    mapAdjustmentController.panUp();
                    break;
                case A:
                    mapAdjustmentController.panLeft();
                    break;    
                case S:
                    mapAdjustmentController.panDown();
                    break;
                case D:
                    mapAdjustmentController.panRight();
                    break;    
            }
        });
       
        //KEYBOARD EVENTS
        
    }

    //THIS WILL INITAILIZE THE STYLE OF EVERYTHING
    private void initStyle() {
        
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
      
        //TOOLBAR STYLES
        clipboardToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        transactionToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        settingsToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        metroLineSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroStationSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        findRouteSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        decorSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        fontSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        navigationSection.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        
        aboutButton.getStyleClass().add(CLASS_FILE_BUTTON);
        cutButton.getStyleClass().add(CLASS_FILE_BUTTON);
	copyButton.getStyleClass().add(CLASS_FILE_BUTTON);
        pasteButton.getStyleClass().add(CLASS_FILE_BUTTON);
        undoButton.getStyleClass().add(CLASS_FILE_BUTTON);
        redoButton.getStyleClass().add(CLASS_FILE_BUTTON);
        
       
        
    }
   
    @Override
    //TODO!
    public void reloadWorkspace(AppDataComponent dataComponent) {
        M3Data data = (M3Data)app.getDataComponent();
        
        //GET THE SELECTED LINE
        MetroLine selectedLine = data.getSelectedMetroLine();
        MetroStation selectedStation = data.getSelectedstation();
        Node selectedNode = data.getSelectedNode();
        
        jTPS transactions = data.getTransactions();
        //SET THERE COLORPICKERS TO REFELCT THEIR COLORS
        BackgroundColorPicker.setValue(data.getBackgroundColor());
        
        undoButton.setDisable(!transactions.canUndo());
        redoButton.setDisable(!transactions.canRedo());
        double width = canvas.getMinWidth();
        double height = canvas.getMinHeight();
        double actualWidth = canvas.getWidth();
        double actualHeight = canvas.getHeight();

        if(startStationDropMenu.getValue() == null || endStationDropMenu.getValue() == null){
            findrouteButton.setDisable(true);
        }
        else{
            findrouteButton.setDisable(false);
        }
        if((width < 250 || height < 250) && !(actualHeight == 0 && actualWidth == 0)){
            decreaseMapSizeButton.setDisable(true);
        }
        else{
            decreaseMapSizeButton.setDisable(false);
        }
        if(canvas.getScaleX() <= .2){
            zoomOutButton.setDisable(true);
        }
        else{
            zoomOutButton.setDisable(false);
        }
        //CHECK IF A STATION IS SELECTED THEN CHANGE RELEVANT BUTTONS
        if (selectedStation != null){
            metroStationsColorPicker.setValue((Color) selectedStation.getStroke());
            removeMetroStationsButton.setDisable(false);
            snapStationToGridButton.setDisable(false);
            moveStationLabelButton.setDisable(false);
            rotateStationLabelButton.setDisable(false);  
            metroStationsSlider.setValue(selectedStation.getRadiusX());
            metroStationsSlider.setDisable(false);
            metroStationsColorPicker.setValue((Color) selectedStation.getFill());
        }
        else{
            removeMetroStationsButton.setDisable(true);
            snapStationToGridButton.setDisable(true);
            moveStationLabelButton.setDisable(true);
            rotateStationLabelButton.setDisable(true);
            metroStationsSlider.setDisable(true);
        }
        
        //CHECK IF A LINE IS SELECTED THEN CHANGE RELEVANT BUTTONS
        if (selectedLine != null){
            removeMetroLineButton.setDisable(false); 
            addStationToLineButton.setDisable(false);
            removeStationFromLineButton.setDisable(false);
            listStationsButton.setDisable(false);
            metroLinesSlider.setValue(selectedLine.getStrokeWidth());
            metroLinesSlider.setDisable(false);
        }
        else{
            removeMetroLineButton.setDisable(true); 
            addStationToLineButton.setDisable(true);
            removeStationFromLineButton.setDisable(true);
            listStationsButton.setDisable(true);
            metroLinesSlider.setDisable(true);
        }
        
       //CHECK IF TEXT
       if(selectedNode != null){
           
           if(selectedNode instanceof DraggableText){
               DraggableText selectedText = (DraggableText) selectedNode;
                removeElementButton.setDisable(false);
                boldButton.setDisable(false); 
                boldButton.setSelected(selectedText.getBoldProperty());
                italicsButton.setDisable(false); 
                italicsButton.setSelected(selectedText.getItalicsProperty());
                fontFamilyComboBox.setDisable(false); 
                fontFamilyComboBox.setValue(selectedText.getFontType());
                fontSizeComboBox.setDisable(false); 
                fontSizeComboBox.setValue(selectedText.getFontSize());
                fontColorPicker.setValue((Color)selectedText.getFill());

           }
           else if(selectedNode instanceof DraggableImage){
                removeElementButton.setDisable(false);
                boldButton.setDisable(true); 
                italicsButton.setDisable(true); 
                fontFamilyComboBox.setDisable(true);  
                fontSizeComboBox.setDisable(true); 
           }
           else{
                removeElementButton.setDisable(true);
                boldButton.setDisable(true); 
                italicsButton.setDisable(true); 
                fontFamilyComboBox.setDisable(true);  
                fontSizeComboBox.setDisable(true); 
           }
           
       }
       else{
            removeElementButton.setDisable(true);
            boldButton.setDisable(true); 
            italicsButton.setDisable(true); 
            fontFamilyComboBox.setDisable(true);  
            fontSizeComboBox.setDisable(true); 
       }
        
    }

    @Override
    public void resetWorkspace() {

    }

   
    
}
