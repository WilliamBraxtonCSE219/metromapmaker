/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import static M3.M3LangaugeProperty.ABOUT_DESCRIPTION;
import static M3.M3LangaugeProperty.ABOUT_TITLE;
import static M3.M3LangaugeProperty.IMAGE_LOAD_ERROR;
import static M3.M3LangaugeProperty.IMAGE_LOAD_MESSAGE;
import static M3.M3LangaugeProperty.IMAGE_TITLE;
import M3.data.DraggableImage;
import M3.data.M3Data;
import M3.data.MetroLine;
import M3.data.MetroStation;
import M3.path.StationGraph;
import M3.transactions.DragNodeTransaction;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.util.List;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

/**
 *
 * @author William
 */
class MapEditController {

    
     AppTemplate app;
     M3Data dataManager;
     AppGUI gui;
    
    public MapEditController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();
    }
    public void showAboutScreen() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(app.getProperty(ABOUT_TITLE));
        alert.setHeaderText(null);
        alert.setContentText(app.getProperty(ABOUT_DESCRIPTION));
        ImageView logo = new ImageView(FILE_PROTOCOL + PATH_IMAGES +  app.getProperty(APP_LOGO));
        alert.setGraphic(logo);
        alert.showAndWait();
    }

    public void processListStations() {
        MetroLine currentLine = dataManager.getSelectedMetroLine();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        StringBuilder listOfStationsBuild = new StringBuilder();
        listOfStationsBuild.append(currentLine.getLineName()).append(" Stations:\n");
        alert.setTitle("List of Stations");
        alert.setHeaderText("These are the list of station on this line, from start to finish");
        List<MetroStation> stationList = currentLine.getStationList();
        for(MetroStation station : stationList){
            listOfStationsBuild.append(station.getStationName()).append("\n");
        }
        alert.setContentText(listOfStationsBuild.toString());
        alert.showAndWait();
    }

    public void processSnapToGrid() {
        MetroStation station = dataManager.getSelectedstation();
        DragNodeTransaction transaction = new DragNodeTransaction(station,station.getX(),station.getY());
        dataManager.getTransactions().addTransaction(transaction);
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    public void processFindRoute(Object from, Object to) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        StringBuilder stationPath = new StringBuilder();
        alert.setTitle("List of Stations");
        alert.setHeaderText("These are the list of station on this line, from start to finish");
        if(from == null || to == null){
            
        }
        else if (from instanceof MetroStation && to instanceof MetroStation){
            StationGraph stationGraph = new StationGraph(dataManager.getLines());
            MetroStation startStation = (MetroStation) from;
            MetroStation endStation = (MetroStation) to;
            
            stationPath.append("Route From ").append(startStation).append(" To ").append(endStation).append(":\n");
             
            List<MetroStation> path = stationGraph.minimumWeightPath(startStation, endStation);
            if(path == null){
               stationPath.append("No such path exist");
            }
            else{
                MetroLine currentLine = null;
                for(int i = 0; i < path.size(); i++){
                    
                    //If its the first station, we must board a line as well
                    if(currentLine == null && i == 0){
                        MetroStation station = path.get(i);
                        MetroStation nextStation = path.get(i+1);
                        List<MetroLine> lineList1 = station.getLineList();
                        List<MetroLine> lineList2 = nextStation.getLineList();
                        for(MetroLine firstLine : lineList1){
                            if(currentLine != null){
                                break;
                            }
                            for(MetroLine secondLine : lineList2){
                                if(firstLine == secondLine){
                                    currentLine = firstLine;
                                    break;
                                }
                            }
                        }
                        stationPath.append("Board the ").append(currentLine).append(" At ").append(station).append("\n");
                    }
                    //If we are at the last station
                    else if(i == path.size() - 1){
                        MetroStation station = path.get(i);
                        stationPath.append("Arrive at your destination: ").append(station).append("\n");
                    }
                    //If its any station, we must check to see if they exist on the same line. If not, we must find another line in common then board it
                    else{
                        MetroStation station = path.get(i);
                        MetroStation nextStation = path.get(i+1);
                        List<MetroLine> stationList = nextStation.getLineList();
                        Boolean lineOnStation = false;
                        for(MetroLine line : stationList){
                            if(currentLine == line){
                                stationPath.append(station).append("\n");
                                lineOnStation = true;
                                break;
                            }
                    }
                        if(!lineOnStation){
                            currentLine = null;
                            List<MetroLine> lineList1 = station.getLineList();
                            List<MetroLine> lineList2 = nextStation.getLineList();
                            for(MetroLine firstLine : lineList1){
                                if(currentLine != null){
                                    break;
                                }
                                for(MetroLine secondLine : lineList2){
                                    if(firstLine == secondLine){
                                        currentLine = firstLine;
                                        break;
                                    }
                                }
                            }
                            stationPath.append("Board the ").append(currentLine).append(" At ").append(station).append("\n");
                        }
                    }
                }
            }
            
        alert.setContentText(stationPath.toString());
        alert.showAndWait(); 
            
        }
    }

    public void processSetImageBackground() {
        // CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	//scene.setCursor(Cursor.CROSSHAIR);
        
	//SET THE FILECHOOSER AND ITS FILTER
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg");
	FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_IMAGES));
	fc.setTitle(app.getProperty(IMAGE_TITLE));
        fc.setSelectedExtensionFilter(imageFilter);
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        if (selectedFile != null) {
            try{
                
                dataManager.setBackgroundImage(selectedFile.toURI().toURL().toString());
               
                gui.updateToolbarControls(false);
                gui.getFileController().markAsEdited(gui);
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(app.getProperty(IMAGE_LOAD_ERROR), app.getProperty(IMAGE_LOAD_MESSAGE));
            }
	// ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    }
    
    public void processChangeBackgroundColor() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Color selectedColor = workspace.getBackgroundColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setBackgroundColor(selectedColor);
	    gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
	} 
        
        workspace.reloadWorkspace(dataManager);

    }

    
}
