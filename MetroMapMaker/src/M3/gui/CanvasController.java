/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.Draggable;
import M3.data.LineEnd;
import M3.data.M3Data;
import M3.data.M3State;
import static M3.data.M3State.ADDING_TO_LINE;
import static M3.data.M3State.DRAGGING_NOTHING;
import static M3.data.M3State.REMOVING_FROM_LINE;
import static M3.data.M3State.SELECTING_NODE;
import M3.data.MetroLine;
import M3.data.MetroStation;
import static M3.transactions.AddLineTransaction.ADD;
import static M3.transactions.AddLineTransaction.REMOVE;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.ui.AppGUI;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import jtps.jTPS;
import M3.transactions.DragNodeTransaction;
import M3.transactions.StationToLineTransaction;

/**
 *
 * @author William
 */
class CanvasController {
    AppTemplate app;
    AppDataComponent data;
    AppGUI gui;

    public CanvasController(AppTemplate initApp) {
        app = initApp;
        data = app.getDataComponent();
        gui = app.getGUI();
    }

    public void processCanvasMousePress(int x, int y) {
        M3Data dataManager = (M3Data) data;
        //IF WE ARE SELECTING SOMETHING
        if(dataManager.isInState(SELECTING_NODE)){
            //SELECT THE TOP NODE
            Node node = dataManager.selectTopNode(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            
            //START DRAGGING IT!
            if (node != null && node instanceof Draggable){
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(M3State.DRAGGING_NODE);
                Draggable selectedDraggableNode = (Draggable) node;
                selectedDraggableNode.start(x,y);
                gui.updateToolbarControls(false);
                gui.getFileController().markAsEdited(gui);
            }
            else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        }
        
        //IF WE ARE ADDING STATIONS TO LINE
        else if(dataManager.isInState(ADDING_TO_LINE)){
            //GET THE TOP NODE
            Node node = dataManager.getTopNode(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            MetroLine selectedLine = dataManager.getSelectedMetroLine();
            if(node instanceof MetroStation){
                MetroStation stationToAdd = (MetroStation) node;
                if(!stationToAdd.isOnLine(selectedLine)){
                    jTPS transactions = dataManager.getTransactions();
                    StationToLineTransaction transaction = new StationToLineTransaction(selectedLine, stationToAdd, StationToLineTransaction.ADD);
                    transactions.addTransaction(transaction);
                    gui.updateToolbarControls(false);
                    gui.getFileController().markAsEdited(gui);                }
            }
            else{
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(SELECTING_NODE);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        }
        
         else if(dataManager.isInState(REMOVING_FROM_LINE)){
            //GET THE TOP NODE
            Node node = dataManager.getTopNode(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            MetroLine selectedLine = dataManager.getSelectedMetroLine();
            if(node instanceof MetroStation){
                //remove the station from the line
                MetroStation stationToRemove = (MetroStation) node;
                if(stationToRemove.isOnLine(selectedLine)){
                    jTPS transactions = dataManager.getTransactions();
                    StationToLineTransaction transaction = new StationToLineTransaction(selectedLine, stationToRemove, StationToLineTransaction.REMOVE);
                    transactions.addTransaction(transaction);
                    gui.updateToolbarControls(false);
                    gui.getFileController().markAsEdited(gui);
                }
            }
            else{
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(SELECTING_NODE);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
         }
        
        M3Workspace workspace = (M3Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        
    }
    
    public void processCanvasMouseDragged(int x, int y) {
        M3Data dataManager = (M3Data) data;
        if (dataManager.isInState(M3State.DRAGGING_NODE)){
            Draggable selectedDraggableNode = (Draggable) dataManager.getSelectedNode();
            selectedDraggableNode.drag(x, y);
            gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
        }
    }
    
    public void processCanvasMouseRelease(int x, int y) {
        M3Data dataManager = (M3Data) data;
        if (dataManager.isInState(M3State.DRAGGING_NODE)){
            Draggable selectedDraggableNode = (Draggable) dataManager.getSelectedNode();
            jTPS transactions = dataManager.getTransactions();
            //May have to use two different transactions for lines
            DragNodeTransaction transaction = new DragNodeTransaction(selectedDraggableNode);
            transactions.addTransaction(transaction);
            dataManager.setState(SELECTING_NODE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
            M3Workspace workspace = (M3Workspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);              
        }else if (dataManager.isInState(M3State.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_NODE);
        }
    }

    
}
