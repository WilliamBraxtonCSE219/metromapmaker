/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.M3Data;
import static M3.data.M3State.ADDING_TO_LINE;
import static M3.data.M3State.REMOVING_FROM_LINE;
import M3.data.MetroLine;
import M3.dialog.EditLineSingleton;
import M3.transactions.LineEditTransaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import jtps.jTPS;

/**
 *
 * @author William
 */
public class NodeEditController {
    AppTemplate app;
    M3Data dataManager;
    AppGUI gui;

    public NodeEditController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();
    }

    public void processAddStationToLine() {
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
        
        dataManager.setState(ADDING_TO_LINE); //To change body of generated methods, choose Tools | Templates.
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveStationFromLine() {
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
        
        dataManager.setState(REMOVING_FROM_LINE); //To change body of generated methods, choose Tools | Templates.
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);    
    }

    public void processMoveStationLabel() {
        dataManager.moveStationLabel();
        
        // ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }

    public void processRotateStationLabel() {
        dataManager.rotateStationLabel();
        
        // ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }

    /**
     * This method processes a users request to bold or unbold text
     */
    public void processBoldText(){
        Scene scene = app.getGUI().getPrimaryScene();
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.ToggleTextBold();
        workspace.reloadWorkspace(dataManager);
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method processs a users request to toggle italics
     */
     public void processItalicsText(){
        Scene scene = app.getGUI().getPrimaryScene();
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.ToggleTextItalics();
        workspace.reloadWorkspace(dataManager);
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }
     
    public void processChangeFontSize(String fontSize) {
        Scene scene = app.getGUI().getPrimaryScene();
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.ChangeTextSize(fontSize);
        workspace.reloadWorkspace(dataManager);
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }

    public void processChangeFontFamily(String textFamily) {
        Scene scene = app.getGUI().getPrimaryScene();
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.ChangeTextFamily(textFamily);
        workspace.reloadWorkspace(dataManager);
        gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);
    }
     
    public void processChangeLineThickness(double thicknessValue) {
        // REMOVE THE SELECTED SHAPE IF THERE IS ONE
        dataManager.changeLineThickness(thicknessValue);
        
        // ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui);   
    }

    public void processChangeStationRadius(double radiusValue) {
        // REMOVE THE SELECTED SHAPE IF THERE IS ONE
        dataManager.changeStationRadius(radiusValue);
        
        // ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
	gui.updateToolbarControls(false);
        gui.getFileController().markAsEdited(gui); 
    }

    public void processEditLine() {
        MetroLine currentLine = dataManager.getSelectedMetroLine();
        jTPS transactionHandler = dataManager.getTransactions();
        ObservableList<Node> lines = dataManager.getLines();
        EditLineSingleton editLineDialog = EditLineSingleton.getSingleton();
        editLineDialog.setColor(currentLine.getLineColor());
        editLineDialog.setLineName(currentLine.getLineName());
        editLineDialog.setCircular(currentLine.isCircular());
        Optional<Pair<Pair<String, Boolean>, Color>> result = editLineDialog.showAndWait();
        if(result.isPresent() && !result.get().getKey().equals("")){
            LineEditTransaction transaction = new LineEditTransaction(currentLine,result.get().getKey().getKey(),result.get().getValue(), result.get().getKey().getValue(), lines);
            transactionHandler.addTransaction(transaction);
            gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
        }
         // ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    
    }
