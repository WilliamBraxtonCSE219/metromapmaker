/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.M3Data;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.event.ActionEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author William
 */
public class MapAdjustmentController {
    AppTemplate app;
    M3Data dataManager;
    AppGUI gui;
    
    public MapAdjustmentController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();
    }
    
    public void processZoomIn() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        if(canvas.getScaleX() > .1){
            canvas.setScaleX(canvas.getScaleX() + .1);
            canvas.setScaleY(canvas.getScaleY() + .1);
        }
	workspace.reloadWorkspace(dataManager);        
    }

    public void processZoomOut() {
         M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        if(canvas.getScaleX() > .1){
            canvas.setScaleX(canvas.getScaleX() - .1);
            canvas.setScaleY(canvas.getScaleY() - .1);
        }
	workspace.reloadWorkspace(dataManager);
    }

    public void processIncreaseMapSize() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        double currentWidth = canvas.getWidth();
        double currentHeight = canvas.getHeight();
        //canvas.setPrefSize(200, 200);
        double x = canvas.getLayoutX();
        double y = canvas.getLayoutY();
        canvas.setMinSize(currentWidth + 50, currentHeight + 50);
        canvas.setMaxSize(currentWidth + 50, currentHeight + 50);
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        workspace.reloadWorkspace(dataManager);
    }

    public void processDecreaseMapSize() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        double currentWidth = canvas.getWidth();
        double currentHeight = canvas.getHeight();
        double x = canvas.getLayoutX();
        double y = canvas.getLayoutY();
        //canvas.setPrefSize(200, 200);
        if(currentWidth >= 250 && currentHeight >= 250){
            canvas.setMinSize(currentWidth - 50, currentHeight - 50);
            canvas.setMaxSize(currentWidth - 50, currentHeight - 50);
            canvas.setLayoutX(x);
            canvas.setLayoutY(y);
        }
        workspace.reloadWorkspace(dataManager);
    }
    
    public void panUp(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        canvas.setTranslateY(canvas.getTranslateY() - 5);

    }
    
    public void panDown(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        canvas.setTranslateY(canvas.getTranslateY() + 5);
    }
    
    public void panLeft(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        canvas.setTranslateX(canvas.getTranslateX() - 5);

    }
    
    public void panRight(){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        canvas.setTranslateX(canvas.getTranslateX() + 5);
    }

    public void processToggleGrid(boolean selected) {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        dataManager.toggleGrid(selected);
    }
    
}
