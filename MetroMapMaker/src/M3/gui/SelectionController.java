/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.M3Data;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import javafx.scene.Scene;

/**
 *
 * @author William
 */
public class SelectionController {
    AppTemplate app;
    M3Data dataManager;
    AppGUI gui;

    
    public SelectionController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();
    }
    
    public void processSelectMetroLine(Object metroLine) {
        Scene scene = app.getGUI().getPrimaryScene();
        dataManager.selectLine((Node) metroLine);
        
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager); 
    }

    public void processSelectStation(Object metroStation) {
        Scene scene = app.getGUI().getPrimaryScene();
        dataManager.selectStation((Node) metroStation);
        
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);     }

    
}
