/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.M3Data;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.paint.Color;

/**
 *
 * @author William
 */
public class ColorController {
    AppTemplate app;
    M3Data dataManager;
    AppGUI gui;
    
    public ColorController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
        gui = app.getGUI();

    }
    

    public void processChangeStationColor() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Color selectedColor = workspace.getMetroStationsColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.changeStationColor(selectedColor);
	    gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
	}
        workspace.reloadWorkspace(dataManager);
    }

    public void processChangeFontColor() {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	Color selectedColor = workspace.getFontColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.changeFontColor(selectedColor);
	    gui.updateToolbarControls(false);
            gui.getFileController().markAsEdited(gui);
	}
    	workspace.reloadWorkspace(dataManager);
    }
    
}
