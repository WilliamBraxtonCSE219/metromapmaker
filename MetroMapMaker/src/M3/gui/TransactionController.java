/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.gui;

import M3.data.M3Data;
import djf.AppTemplate;
import javafx.scene.Scene;

/**
 *
 * @author William
 */
public class TransactionController {
    AppTemplate app;
    M3Data dataManager;
    
    public TransactionController(AppTemplate initApp) {
	app = initApp;
	dataManager = (M3Data)app.getDataComponent();
    } 
    
     public void processUndo(){
        Scene scene = app.getGUI().getPrimaryScene();
        
	// CHANGE THE STATE
        dataManager.undoAction();
	//dataManager.setState(golState.SELECTING_SHAPE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    public void processRedo(){
         Scene scene = app.getGUI().getPrimaryScene();
        
	// CHANGE THE STATE
        dataManager.redoAction();
	//dataManager.setState(golState.SELECTING_SHAPE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
}
