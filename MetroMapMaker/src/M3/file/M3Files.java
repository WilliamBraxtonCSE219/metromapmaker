/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.file;

import static M3.data.Draggable.IMAGE;
import static M3.data.Draggable.TEXT;
import M3.data.DraggableImage;
import M3.data.LineEnd;
import M3.data.M3Data;
import M3.data.MetroLine;
import M3.data.MetroStation;
import M3.data.DraggableText;
import M3.gui.M3Workspace;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static djf.settings.AppPropertyType.WORK_FILE_EXT;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jtps.jTPS;
import properties_manager.PropertiesManager;

/**
 *
 * @author William Braxton
 */
public class M3Files implements AppFileComponent{
    // FOR JSON LOADING
    static final String JSON_NAME = "name";
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_BG_IMAGE = "background_image";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_SHAPES = "shapes";
    static final String JSON_SHAPE = "shape"; 
    static final String JSON_LINES = "lines"; 
    static final String JSON_STATIONS = "stations"; 
    static final String JSON_IMAGES = "images"; 
    static final String JSON_LABELS = "labels";
    static final String JSON_STATIONS_LIST = "station_names";     
    static final String JSON_ROTATION = "rotation value";     
    static final String JSON_LABEL_POSITION = "label position";     
    static final String JSON_THICKNESS = "thickness";
    static final String JSON_TYPE = "type";
    static final String JSON_TEXT = "text";
    static final String JSON_SIZE = "font size";
    static final String JSON_FONT = "font";
    static final String JSON_BOLD = "bold";
    static final String JSON_ITALICS = "italics";   
    static final String JSON_IMAGE = "image";       
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_RADIUS = "radius";
    static final String JSON_START_X = "line start x";
    static final String JSON_START_Y = "line start y";
    static final String JSON_END_X = "line end x";
    static final String JSON_END_Y = "line end y";        
    static final String JSON_WIDTH = "width";
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_HEIGHT = "height";
    static final String JSON_COLOR = "color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    
    AppTemplate app;
    String currentFile;
    
    public M3Files(AppTemplate app){
        this.app = app;
    }
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        M3Data M3Data = (M3Data) data;
        
        // FIRST THE BACKGROUND COLOR
	Color bgColor = M3Data.getBackgroundColor();
        String imageUrl = M3Data.getBackgroundImage();
        if (imageUrl == null || imageUrl.equals("")){
            imageUrl = "null";
        }
	JsonObject bgColorJson = makeJsonColorObject(bgColor);
        
        String[] pathArray = filePath.split("\\\\");
        // NOW BUILD THE JSON OBJCTS TO SAVE
        //TWO ARRAYS, ONE FOR LINES, ONE FOR STATIONS
        JsonArrayBuilder metroLines = Json.createArrayBuilder();
        JsonArrayBuilder metroStations = Json.createArrayBuilder();
        JsonArrayBuilder shapes = Json.createArrayBuilder();
	//GET THE CANVAS NODES SO WE CAN LOOP THROUGH THEM!
        ObservableList<Node> mapNodes = M3Data.getShapes();
        
        for (Node currentNode : mapNodes){
            if (currentNode instanceof MetroLine){
                
                JsonObject lineObject = makeLineObject(currentNode);
                metroLines.add(lineObject); //ADD IT TO THE LINE ARRAY
                        
            }
            else if (currentNode instanceof MetroStation){
                JsonObject stationObject = makeStationObject(currentNode);
                metroStations.add(stationObject);//ADD OBJECT TO LIST OF STATIONS
            }
            else if (currentNode instanceof DraggableImage){
                DraggableImage image = (DraggableImage) currentNode;
                JsonObject imageJson = Json.createObjectBuilder()
                        .add(JSON_X, image.getX())
                        .add(JSON_Y, image.getY())
                        .add(JSON_TYPE, image.getShapeType())
                        .add(JSON_IMAGE, image.getURL())
                        .add(JSON_WIDTH, image.getWidth())
                        .add(JSON_HEIGHT, image.getHeight()).build();
                 	shapes.add(imageJson);
                        
            }
            else if(currentNode instanceof DraggableText){
                DraggableText text = (DraggableText) currentNode;
                JsonObject textColorJson = makeJsonColorObject((Color)text.getFill());
                double outlineThickness = text.getStrokeWidth();
                String textTxt = text.getText();
                JsonObject textJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, text.getShapeType())
                        .add(JSON_X, text.getX())
                        .add(JSON_Y, text.getY())
                        .add(JSON_COLOR, textColorJson)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness)
                        .add(JSON_BOLD, text.getBoldProperty())
                        .add(JSON_FONT, text.getFontType())
                        .add(JSON_ITALICS, text.getItalicsProperty())
                        .add(JSON_SIZE, text.getFontSize())
                        .add(JSON_TEXT, textTxt).build();
                 	shapes.add(textJson);
            }
        }
        
        
        
         
        //PUT IT ALL TOGETHER
        JsonObject mapInfo = Json.createObjectBuilder()
                .add(JSON_NAME, pathArray[pathArray.length - 1]) //This should give the file name
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_BG_IMAGE, imageUrl)
                .add(JSON_LINES, metroLines)
                .add(JSON_STATIONS, metroStations)
                .add(JSON_SHAPES, shapes)
                .build();
        
         // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(mapInfo);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath  +" Metro"+ props.getProperty(WORK_FILE_EXT));
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(mapInfo);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath +" Metro" + props.getProperty(WORK_FILE_EXT));
	pw.write(prettyPrinted);
	pw.close();
        
    }
    /**
     * helper method for save function
     * @param currentNode
     * @return 
     */
    private JsonObject makeLineObject(Node currentNode){
         MetroLine currentLine = (MetroLine) currentNode;
                JsonObject lineColorJson = makeJsonColorObject((Color)currentLine.getLineColor());
                
                LineEnd startSeg = currentLine.getStartSegment();
                LineEnd endSeg = currentLine.getEndSegment();

                //GET THE STATION LIST TO PUT INTO A STATIONS ARRAY FOR THE LINE
                List<MetroStation> stations = currentLine.getStationList();
                JsonArrayBuilder lineStations = Json.createArrayBuilder();
                for (MetroStation currentStation : stations){ //ADD EACH STATION
                    lineStations.add(currentStation.getStationName());
                }
                
                JsonObject lineObject = Json.createObjectBuilder()
                        .add(JSON_NAME, currentLine.getLineName())
                        .add(JSON_COLOR, lineColorJson)
                        .add(JSON_THICKNESS, currentLine.getStrokeWidth())
                        .add(JSON_START_X, startSeg.getX())
                        .add(JSON_START_Y, startSeg.getY())
                        .add(JSON_END_X, endSeg.getX())
                        .add(JSON_END_Y, endSeg.getY())
                        .add(JSON_STATIONS_LIST, lineStations)
                        .build();
                return lineObject;
    }
    /**
     * Helper method for save funciton. Makes a station json object
     * @param currentNode
     * @return 
     */
    private JsonObject makeStationObject(Node currentNode){
         MetroStation currentStation = (MetroStation) currentNode;
                
                String name = currentStation.getStationName();
                double x = currentStation.getX();
                double y = currentStation.getY();
                JsonObject stationColorJson = makeJsonColorObject((Color)currentStation.getFill());
                double stationRadius = currentStation.getRadiusX();
                JsonObject stationObject = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_COLOR, stationColorJson)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_RADIUS, stationRadius)
                        .add(JSON_ROTATION, currentStation.getRotation())
                        .add(JSON_LABEL_POSITION, currentStation.getLabelPosition())
                        .build();
                return stationObject;
    }

    /**
     * First we load stations, then lines, then add the stations for each line
     * @param data
     * @param filePath
     * @throws IOException 
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	M3Data dataManager = (M3Data)data;
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
                
	dataManager.resetData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
	ObservableList<Node> mapNodes = dataManager.getShapes();
        ObservableList<Node> stations = dataManager.getStations();
        ObservableList<Node> lines = dataManager.getLines();
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// LOAD THE BACKGROUND COLOR
	Color bgColor = loadColor(json, JSON_BG_COLOR);
        String imageUrl = json.getString(JSON_BG_IMAGE);
        dataManager.setBackgroundImage(imageUrl);
	dataManager.setBackgroundColor(bgColor);
        jTPS transactions = dataManager.getTransactions();
        transactions.clear();
	
	// AND NOW LOAD ALL THE SHAPES
        //FIRST STATIONS
	JsonArray jsonStationArray = json.getJsonArray(JSON_STATIONS);
	for (int i = 0; i < jsonStationArray.size(); i++) {
	    JsonObject jsonShape = jsonStationArray.getJsonObject(i);
	    MetroStation station = loadStation(jsonShape);
            //mapNodes.addAll(station, station.getText());
	    stations.add(station);
	}
        
        //THEN LINES, NOTE THAT THEY ARE NOT ADDED TO THE CANVAS BECAUSE WE
        //WANT STATIONS ON TOP OF LINES!
	JsonArray jsonLineArray = json.getJsonArray(JSON_LINES);
        for (int i = 0; i < jsonLineArray.size(); i++) {
	    JsonObject jsonShape = jsonLineArray.getJsonObject(i);
	    MetroLine line = loadMetroLine(jsonShape);
            lines.add(line);
            JsonArray stationList = jsonShape.getJsonArray(JSON_STATIONS_LIST);
            List<MetroStation> lineStations = new ArrayList<>();
            //GET ALL THE STATIONS APART OF THIS LINE AND ADD THEM!
            for(int j = 0; j < stationList.size(); j++){
                String stationName = stationList.getString(j);
                for(Node station : stations){
                    MetroStation currentStation = (MetroStation) station;
                    if (currentStation.getStationName().equals(stationName)){
                        //ADD THE STATION THEN BREAK 
                        lineStations.add(currentStation);
                        break;
                    }
                }
            }
            //SET THE STATIONS LIST
            line.setStationList(lineStations);
        } 
        //THEN LOAD IMAGES AND PUT THEM ON THE CANVAS!
        JsonArray jsonImageArray = json.getJsonArray(JSON_SHAPES);
        for (int i = 0; i < jsonImageArray.size(); i++){
                JsonObject jsonShape = jsonImageArray.getJsonObject(i);     
                double x = getDataAsDouble(jsonShape, JSON_X);
                double y = getDataAsDouble(jsonShape, JSON_Y);
                String shapeType = jsonShape.getString(JSON_TYPE);
                if(shapeType.equals(IMAGE)){
                    DraggableImage image = new DraggableImage(jsonShape.getString(JSON_IMAGE));
                    double width = getDataAsDouble(jsonShape, JSON_WIDTH);
                    double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
                    image.setLocationAndSize(x, y, width, height);
                    mapNodes.add(image);
                }
                else if(shapeType.equals(TEXT)){
                    DraggableText textShape = new DraggableText(jsonShape.getString(JSON_TEXT));
                    textShape.setFontType(jsonShape.getString(JSON_FONT));
                    textShape.setFontSize(jsonShape.getString(JSON_SIZE));
                    textShape.setBoldProperty(jsonShape.getBoolean(JSON_BOLD));
                    textShape.setItalicsProperty(jsonShape.getBoolean(JSON_ITALICS));
                    textShape.setX(x);
                    textShape.setY(y);
                    // THEN LOAD ITS FILL AND OUTLINE PROPERTIES
                    Color fillColor = loadColor(jsonShape, JSON_COLOR);
                    textShape.setFill(fillColor);
                    mapNodes.add(textShape);
                }
        }
        
        //NOW LETS ADD LINES TO THE CANVAS, THEN THE STATIONS!
        lines.stream().map((node) -> (MetroLine) node).forEachOrdered((line) -> {
            mapNodes.addAll(line, line.getStartSegment(), line.getEndSegment());
            mapNodes.addAll(line.getStartSegment().getText(), line.getEndSegment().getText());
        });
        
        stations.stream().map((node) -> (MetroStation) node).forEachOrdered((station) -> {
            mapNodes.addAll(station, station.getText());
        });
        
        workspace.reloadWorkspace(data);
        
    }
    
    
    private MetroLine loadMetroLine(JsonObject lineObj){
        String lineName = lineObj.getString(JSON_NAME);
        Color lineColor = loadColor(lineObj, JSON_COLOR);
        double lineThickness = getDataAsDouble(lineObj, JSON_THICKNESS);
        double startX = getDataAsDouble(lineObj, JSON_START_X);
        double startY = getDataAsDouble(lineObj, JSON_START_Y);
        double endX = getDataAsDouble(lineObj, JSON_END_X);
        double endY = getDataAsDouble(lineObj, JSON_END_Y);
        LineEnd startSeg = new LineEnd((int) startX, (int) startY);
        LineEnd endSeg = new LineEnd((int) endX, (int) endY);
        MetroLine newLine = new MetroLine(lineName, lineColor, startSeg, endSeg);
        newLine.setStrokeWidth(lineThickness);
        return newLine;
    }
    
    private MetroStation loadStation(JsonObject stationObj){
        double x = getDataAsDouble(stationObj, JSON_X);
        double y = getDataAsDouble(stationObj, JSON_Y);
        double radius = getDataAsDouble(stationObj, JSON_RADIUS);
        String stationName = stationObj.getString(JSON_NAME);
        Color stationColor = loadColor(stationObj, JSON_COLOR);
        int rotation = stationObj.getInt(JSON_ROTATION);
        int labelPos = stationObj.getInt(JSON_LABEL_POSITION);
        MetroStation station = new MetroStation(stationName);
        station.setX((int) x);
        station.setY((int) y);
        station.setFill(stationColor);
        station.setRadius(radius);
        station.setLabelPosition(labelPos);
        station.setRotation(rotation);
        return station;
        
    }

      // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        //ADD JSON LOADING!
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        M3Data M3Data = (M3Data) data;
        //Unhighilite selected shape for exporting
        Node selectedNode = M3Data.getSelectedNode();
        if(selectedNode != null){
            M3Data.unhighlightShape(M3Data.getSelectedNode());
        }
        
	screenshotCanvas(filePath);
        
        String[] pathArray = filePath.split("\\\\");
        
        //TWO ARRAYS, ONE FOR LINES, ONE FOR STATIONS
        JsonArrayBuilder metroLines = Json.createArrayBuilder();
        JsonArrayBuilder metroStations = Json.createArrayBuilder();

        //GET THE CANVAS NODES SO WE CAN LOOP THROUGH THEM!
        ObservableList<Node> mapNodes = M3Data.getShapes();
        
        for (Node currentNode : mapNodes){
            if (currentNode instanceof MetroLine){
                MetroLine currentLine = (MetroLine) currentNode;
                JsonObject lineColorJson = makeJsonColorObject((Color)currentLine.getLineColor());
                
                //GET THE STATION LIST TO PUT INTO A STATIONS ARRAY FOR THE LINE
                List<MetroStation> stations = currentLine.getStationList();
                JsonArrayBuilder lineStations = Json.createArrayBuilder();
                for (MetroStation currentStation : stations){ //ADD EACH STATION
                    lineStations.add(currentStation.getStationName());
                }
                
                //MAKE OUR LINE OBJECT ALERT!! MAY HAVE TO ADD CIRCULAR FIELD
                JsonObject lineObject = Json.createObjectBuilder()
                        .add(JSON_NAME, currentLine.getLineName())
                        .add(JSON_CIRCULAR, currentLine.isCircular())
                        .add(JSON_COLOR, lineColorJson)
                        .add(JSON_STATIONS_LIST, lineStations)
                        .build();
                metroLines.add(lineObject); //ADD IT TO THE LINE ARRAY
                        
            }
            else if (currentNode instanceof MetroStation){
                MetroStation currentStation = (MetroStation) currentNode;
                
                String name = currentStation.getStationName();
                double x = currentStation.getX();
                double y = currentStation.getY();
                
                JsonObject stationObject = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .build();
                metroStations.add(stationObject);
            }
        }
        
         
        //PUT IT ALL TOGETHER
        JsonObject mapInfo = Json.createObjectBuilder()
                .add(JSON_NAME, pathArray[pathArray.length - 1]) //This should give the file name
                .add(JSON_LINES, metroLines)
                .add(JSON_STATIONS, metroStations)
                .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(mapInfo);
	jsonWriter.close();

        String jsonFileName = filePath  + " Metro" + ".json";
	// INIT THE WRITER
	OutputStream os = new FileOutputStream(jsonFileName);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(mapInfo);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFileName);
	pw.write(prettyPrinted);
	pw.close();
        
        //Rehighlight the unhighlighted shape
        if(selectedNode != null){
            M3Data.highlightShape(M3Data.getSelectedNode());
        }
    }
    
    private JsonObject makeJsonColorObject(Color color) {
	JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }
    /**
     * this takes a screenshot
     */
    private void screenshotCanvas(String filePath){
        M3Workspace workspace = (M3Workspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        double XScale = canvas.getScaleX();
        double YScale = canvas.getScaleY();
        canvas.setScaleX(1);
        canvas.setScaleY(1);
        
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
	
        canvas.setScaleX(XScale);
        canvas.setScaleY(YScale);
        
        File file = new File(filePath + " Metro" + ".png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
