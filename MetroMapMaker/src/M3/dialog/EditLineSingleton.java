/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.dialog;

import java.util.Optional;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.util.Pair;

/**
 *
 * @author William
 */
public final class EditLineSingleton {
    static EditLineSingleton singleton;
    private Dialog stationDialog;
    private TextField lineName;
    private CheckBox circular;
    private ColorPicker colors;
    
    private EditLineSingleton(){
        init();
    }
     
    public static EditLineSingleton getSingleton() {
	if (singleton == null)
	    singleton = new EditLineSingleton();
	return singleton;
    }
    
    public void init(){
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        lineName = new TextField();
        lineName.setPromptText("Line Name");
        colors = new ColorPicker();
        colors.setValue(Color.BLACK);
        circular = new CheckBox();
        
        grid.add(new Label("Line Name:"), 0,0);
        grid.add(lineName, 1, 0);
        grid.add(new Label("Line Color:"), 0,1);
        grid.add(colors, 1,1);
        grid.add(new Label("Circular?"), 0, 2);
        grid.add(circular, 1, 2);

        stationDialog = new Dialog();
        stationDialog.setTitle("Enter Line Name");
        stationDialog.setContentText("Please enter a line name:");
        stationDialog.getDialogPane().setContent(grid);
        
        ButtonType okayBtn = ButtonType.OK;
        //MAKE THE BUTTONS
        stationDialog.getDialogPane().getButtonTypes().addAll(okayBtn, ButtonType.CANCEL);
        
        //SET THE RESULT
        stationDialog.setResultConverter(dialogButton -> {
            if (dialogButton == okayBtn) {
            return new Pair<>(new Pair<>(lineName.getText(), circular.selectedProperty().get()), colors.getValue());
            //return new Pair<>(lineName.getText(), colors.getValue());
        }
        return null;
        });
    }
    
    public void setLineName(String text){
        lineName.setText(text);
    }
    
    public void setColor(Color color){
        colors.setValue(color);
    }
    
    public void setCircular(boolean circ){
        circular.selectedProperty().set(circ);
    }
    
    public Optional<Pair<Pair<String,Boolean>, Color>> showAndWait(){
        Optional<Pair<Pair<String,Boolean>, Color>> result = stationDialog.showAndWait();
        lineName.clear();
        colors.setValue(Color.BLACK);
        return result;
    }
}
