/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.dialog;

import java.util.Optional;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author William
 */
public final class AddStationDialogSingleton {
    
    static AddStationDialogSingleton singleton;
    private TextInputDialog stationDialog;
    
    private AddStationDialogSingleton(){
        init();
    }
     
    public static AddStationDialogSingleton getSingleton() {
	if (singleton == null)
	    singleton = new AddStationDialogSingleton();
	return singleton;
    }
    
    public void init(){
        stationDialog = new TextInputDialog();
        stationDialog.setTitle("Enter Station Name");
        stationDialog.setContentText("Please enter a Station name:");
    }
    
    public Optional<String> showAndWait(){
        stationDialog.getEditor().clear();
        Optional<String> result = stationDialog.showAndWait();
        return result;
    }
}
