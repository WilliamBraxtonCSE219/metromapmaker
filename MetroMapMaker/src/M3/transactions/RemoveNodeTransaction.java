/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author William Braxton
 */
public class RemoveNodeTransaction implements jTPS_Transaction{
   
    private Node shape;
    private ObservableList<Node> shapes;
    
    public RemoveNodeTransaction(Node newNode, ObservableList<Node> sceneShapes){
        shape = newNode;
        this.shapes = sceneShapes;
    }
    
    @Override
    public void doTransaction() {
        shapes.remove(shape);
    }

    @Override
    public void undoTransaction() {
        shapes.add(shape);
    }
}
