/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.LineEnd;
import M3.data.M3Data;
import M3.data.MetroLine;
import M3.data.MetroStation;
import M3.gui.M3Workspace;
import djf.AppTemplate;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class AddLineTransaction implements jTPS_Transaction{

    private final MetroLine line;
    private final ObservableList<Node> shapes;
    private final ObservableList<Node> lines;
    private final String typeOfEdit;
    public static final String REMOVE = "remove";
    public static final String ADD = "add";
    private final LineEnd startSeg;
    private final LineEnd endSeg;
    private final List<MetroStation> listOfStations;
    private final int index;


    public AddLineTransaction(MetroLine newNode, ObservableList<Node> lines, ObservableList<Node> mapNodes, String typeOfEdit) {
        this.line = newNode;
        this.shapes = mapNodes;
        this.lines = lines;
        this.typeOfEdit = typeOfEdit;
        if(typeOfEdit.equals(ADD)){
            startSeg = line.getStartSegment();
            endSeg = line.getEndSegment();
            listOfStations = null;
            index = 0;
        }
        else if(typeOfEdit.equals(REMOVE)){
            startSeg = line.getStartSegment();
            endSeg = line.getEndSegment();
            listOfStations = line.getStationList();
            index = lines.indexOf(line);
        }
        else{
            startSeg = null;
            endSeg = null;
            listOfStations = null;
            index = 0;
        }
    }
    
    @Override
    public void doTransaction() {
        if(typeOfEdit.equals(ADD)){
            lines.add(line);
            shapes.addAll(line, startSeg,endSeg, startSeg.getText(), endSeg.getText());
        }
        else if (typeOfEdit.equals(REMOVE)){
            line.processRemoval();
            shapes.remove(line);
            lines.remove(line);
            shapes.removeAll(line.getStartSegment(), line.getStartSegment().getText());
            shapes.removeAll(line.getEndSegment(), line.getEndSegment().getText());
        }
        
        
    }       

    @Override
    public void undoTransaction() {
        if(typeOfEdit.equals(ADD)){
            lines.remove(line);
            shapes.removeAll(line, startSeg,endSeg, startSeg.getText(), endSeg.getText());
        }
        else if (typeOfEdit.equals(REMOVE)){
            line.setStationList(listOfStations);
            shapes.add(index, line);
            lines.add(line);
            shapes.addAll(line.getStartSegment(), line.getStartSegment().getText());
            shapes.addAll(line.getEndSegment(), line.getEndSegment().getText());
        }
    }
}
