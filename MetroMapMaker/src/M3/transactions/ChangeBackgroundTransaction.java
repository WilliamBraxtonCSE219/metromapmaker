/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.M3Data;
import M3.gui.M3Workspace;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class ChangeBackgroundTransaction implements jTPS_Transaction{
    private final Pane canvas;
    private final Background newBackground;
    private final Background oldBackground;
    private final String oldImage;
    private final String newImage;
    private final M3Data data;
    private final Color oldColor;
    private final Color newColor;
    
    public ChangeBackgroundTransaction(Color backgroundColor, String imageUrl, Pane canvas, M3Data data){
        this.canvas = canvas;     
        BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
        this.newBackground = new Background(fill);
        this.oldBackground = canvas.getBackground();
        this.data = data;
        oldColor = data.getBackgroundColor();
        newColor = backgroundColor;
        oldImage = data.getBackgroundImage();
        newImage = imageUrl;
    }

    @Override
    public void doTransaction() {
        /*canvas.setStyle("-fx-background-color: #" + Integer.toHexString(newColor.hashCode()) + "," + "linear-gradient(from 0px 0px to 20px 0px, repeat, black 0%, transparent 5%),\n" +
"linear-gradient(from 0px 0px to 0px 20px, repeat, black 0%, transparent 5%);"
                        + "-fx-background-image: " + "url(" + newImage + ");" );*/
        canvas.setStyle("-fx-background-color: #" + Integer.toHexString(newColor.hashCode()) + ";"
                        + "-fx-background-image: " + "url(" + newImage + ");" );
        data.setBackgroundColorProperty(newColor);
        data.setBackgroundImageProperty(newImage);
    }

    @Override
    public void undoTransaction() {
        /*canvas.setStyle("-fx-background-color: #" + Integer.toHexString(oldColor.hashCode()) + "," + "linear-gradient(from 0px 0px to 20px 0px, repeat, black 5%, transparent 5%),\n" +
"linear-gradient(from 0px 0px to 0px 20px, repeat, black 5%, transparent 5%);"
                        + "-fx-background-image: " + "url(" + oldImage + ");");*/
        canvas.setStyle("-fx-background-color: #" + Integer.toHexString(oldColor.hashCode()) + ";"
                        + "-fx-background-image: " + "url(" + oldImage + ");");
        data.setBackgroundColorProperty(oldColor);
        data.setBackgroundImageProperty(oldImage);
    }
}
