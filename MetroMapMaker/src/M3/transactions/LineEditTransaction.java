/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.M3Data;
import M3.data.MetroLine;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class LineEditTransaction implements jTPS_Transaction{
    private final MetroLine line;
    private final String oldName;
    private final String newName;
    private final Color newColor;
    private final Color oldColor;
    private final double oldX;
    private final double oldY;
    ObservableList<Node> lines;
    private final boolean circularNew;
    private final boolean circularOld;
    
    public LineEditTransaction(MetroLine line, String newName, Color lineColor, boolean ciruclar, ObservableList<Node> lines){
       this.line = line;
       this.oldName = line.getLineName();
       this.newName = newName;
       this.oldColor = line.getLineColor();
       this.newColor = lineColor;
       this.lines = lines;
       this.circularNew = ciruclar;
       this.circularOld = line.isCircular();
       this.oldX = line.getStartSegment().getX();
       this.oldY = line.getStartSegment().getY();
    }

    @Override
    public void doTransaction() {
        line.setLineColor(newColor);
        line.setLineName(newName);
        line.setCircular(circularNew);
        lines.remove(line);
        lines.add(line);
        
    }

    @Override
    public void undoTransaction() {
        line.setLineColor(oldColor);
        line.setLineName(oldName);
        line.setCircular(circularOld);
        line.getStartSegment().setX((int) oldX);
        line.getStartSegment().setY((int) oldY);
        lines.remove(line);
        lines.add(line);
    }
}
