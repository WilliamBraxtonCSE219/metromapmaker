/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.M3Data;
import M3.data.MetroLine;
import M3.data.MetroStation;
import M3.gui.M3Workspace;
import djf.AppTemplate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class AddStationTransaction implements jTPS_Transaction{

    private final MetroStation station;
    private final ObservableList<Node> shapes;
    private final ObservableList<Node> stations;
    private final Text stationText;
    private final String typeOfEdit;
    private final List<MetroLine> listOfLines;
    private List<List<MetroStation>> metroLineListOfStations = new ArrayList();
    public static final String ADD = "add";
    public static final String REMOVE = "remove";


    public AddStationTransaction(MetroStation newNode, ObservableList<Node> stations, 
            ObservableList<Node> mapNodes, String typeOfEdit) {
        station = newNode;
        this.typeOfEdit = typeOfEdit;
        this.shapes = mapNodes;
        this.stations = stations;
        this.stationText = newNode.getText();
        listOfLines = station.getLineList();
        for(MetroLine line : listOfLines){
                List<MetroStation> listToAdd = new ArrayList(line.getStationList());
                metroLineListOfStations.add(listToAdd);
        }
    }
    
    @Override
    public void doTransaction() {
        if(typeOfEdit.equals(ADD)){
            shapes.add(station);
            stations.add(station);
            shapes.add(stationText);
        }
        else if (typeOfEdit.equals(REMOVE)){
            
            station.processRemoval();
            stations.remove(station);
            shapes.remove(station);
            shapes.remove(station.getText());
        }   
    }       

    @Override
    public void undoTransaction() {
        if(typeOfEdit.equals(ADD)){
            shapes.remove(station);
            stations.remove(station);
            shapes.remove(stationText);
        }
        else if (typeOfEdit.equals(REMOVE)){
            stations.add(station);
            shapes.add(station);
            shapes.add(station.getText());
             for(int i = 0; i < listOfLines.size(); i++){
                 MetroLine line = listOfLines.get(i);
                 List<MetroStation> stationList = new ArrayList(metroLineListOfStations.get(i));
                 line.setStationListOnly(stationList);
             }
        }
    }
}
