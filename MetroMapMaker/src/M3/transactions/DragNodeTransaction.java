/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.Draggable;
import M3.data.MetroStation;
import jtps.jTPS_Transaction;

/**
 *This is the transaction class for dragging shit
 * @author Willliam
 */
public class DragNodeTransaction implements jTPS_Transaction{
    private final double oldX;
    private final double newX;
    private final double oldY;
    private final double newY;
    private Boolean isFresh;
    private final String typeOfTransaction;
    private final Draggable shape;
    private final String DRAG = "drag";
    private final String SNAP = "snap";
    
    public DragNodeTransaction(Draggable shape){
        isFresh = true;
        typeOfTransaction = DRAG;
        this.shape = shape;
        oldX = shape.getLastX();
        oldY = shape.getLastY();
        newX = shape.getX();
        newY = shape.getY();
    }
    
    public DragNodeTransaction(MetroStation shape, double x, double y){
        isFresh = false;
        typeOfTransaction = SNAP;
        shape.snap();
        this.shape = shape;
        oldX = x;
        oldY = y;
        newX = shape.getX();
        newY = shape.getY();
    }
    @Override
    public void doTransaction() {
            if (isFresh){
                isFresh = false;
            }
            else{
                shape.setX((int) newX);
                shape.setY((int) newY);
            }
    }

    @Override
    public void undoTransaction() {
        shape.setX((int) oldX);
        shape.setY((int) oldY);
    }
    
}
