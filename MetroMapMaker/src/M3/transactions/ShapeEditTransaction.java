/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.MetroLine;
import M3.data.MetroStation;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *This class handles all transactions on shape edits
 * @author William
 */
public class ShapeEditTransaction implements jTPS_Transaction{
    private String typeOfEdit;
    private final Node shape;
    private double oldValue;
    private double newValue;
    public static final String LINE_STROKE = "Fill Color";
    public static final String STATION_RADIUS = "Station Radius";
    
    public ShapeEditTransaction(MetroLine shape, double newSetting){
        this.shape = shape;
        this.typeOfEdit = LINE_STROKE;  
        this.newValue = newSetting;
        this.oldValue = shape.getStrokeWidth();
    }
    
    public ShapeEditTransaction(MetroStation shape, double newSetting){
        this.shape = shape;
        this.typeOfEdit = STATION_RADIUS;   
        this.newValue = newSetting;
        this.oldValue = shape.getRadiusX();
    }
    
    @Override
    public void doTransaction() {
        if (typeOfEdit.equals(LINE_STROKE)){
            MetroLine line = (MetroLine) shape;
            line.setStrokeWidth(newValue);
        }
        if (typeOfEdit.equals(STATION_RADIUS)){
            MetroStation station = (MetroStation) shape;
            station.setRadius(newValue);
        } 
    }

    @Override
    public void undoTransaction() {
        if (typeOfEdit.equals(LINE_STROKE)){
            MetroLine line = (MetroLine) shape;
            line.setStrokeWidth(oldValue);
        }
        if (typeOfEdit.equals(STATION_RADIUS)){
            MetroStation station = (MetroStation) shape;
            station.setRadius(oldValue);
        }
    }
    
}
