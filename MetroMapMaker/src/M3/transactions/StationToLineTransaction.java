/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.DraggableText;
import M3.data.MetroLine;
import M3.data.MetroStation;
import javafx.scene.Node;
import javafx.scene.text.Font;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class StationToLineTransaction implements jTPS_Transaction {
    private final String typeOfEdit;
    private final MetroLine line;
    private final MetroStation station;
    public static final String ADD = "add to line";
    public static final String REMOVE = "remove from line";
    
    public StationToLineTransaction(MetroLine line, MetroStation station, String typeOfEdit){
        this.typeOfEdit = typeOfEdit;
        this.line = line;
        this.station = station;
    }

    @Override
    public void doTransaction() {
        if(typeOfEdit.equals(ADD)){
            line.addStation(station);
        }
        else if(typeOfEdit.equals(REMOVE)){
            line.removeStation(station);
        }
        
    }

    @Override
    public void undoTransaction() {
        if(typeOfEdit.equals(ADD)){
            line.removeStation(station);
        }
        else if(typeOfEdit.equals(REMOVE)){
            line.addStation(station);
        }    
    }
}
