/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;
import M3.data.DraggableImage;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import jtps.jTPS_Transaction;
/**
 *
 * @author William
 */
public class AddNodeTransaction implements jTPS_Transaction{
    private Node shape;
    private ObservableList<Node> shapes;
    private String typeOfEdit;
    private final String IMAGE = "image";
    private final String NODE = "node";

    public AddNodeTransaction(DraggableImage newNode, ObservableList<Node> sceneShapes ){
        shape = newNode;
        this.shapes = sceneShapes;
        this.typeOfEdit = IMAGE;
    }
    
    public AddNodeTransaction(Node newNode, ObservableList<Node> sceneShapes ){
        shape = newNode;
        this.shapes = sceneShapes;
        this.typeOfEdit = NODE;
    }
    
    @Override
    public void doTransaction() {
            shapes.add(shape);
    }

    @Override
    public void undoTransaction() {
            shapes.remove(shape);
    }
    
}
