/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.DraggableText;
import M3.data.MetroLine;
import M3.data.MetroStation;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class ColorChangeTransaction implements jTPS_Transaction {
    private DraggableText text;
    private MetroStation station;
    private MetroLine line;
    private String typeOfEdit;
    private Color oldColor; //used if type edit is font or size
    private Color newColor; //Used if type edit is font or size
    public static final String STATION = "Bold";
    public static final String TEXT = "Italics";
    
    public ColorChangeTransaction(MetroStation station, Color newColor){
        typeOfEdit = STATION;
        this.text = null;
        this.station = station;
        this.line = null;
        oldColor = (Color) station.getFill();
        this.newColor = newColor;
    }
    
    public ColorChangeTransaction(DraggableText text, Color newColor){
        typeOfEdit = TEXT;
        this.text = text;
        this.station = null;
        this.line = null;
        oldColor = (Color) text.getFill();
        this.newColor = newColor;
    }
    
   
    @Override
    public void doTransaction() {
       if (typeOfEdit.equals(STATION)){
            station.setFill(newColor);
        }
        else if (typeOfEdit.equals(TEXT)){
            text.setFill(newColor);

        }      
    }

    @Override
    public void undoTransaction() {
        if (typeOfEdit.equals(STATION)){
            station.setFill(oldColor);
        }
        else if (typeOfEdit.equals(TEXT)){
            text.setFill(oldColor);

        }
    }
    
}
