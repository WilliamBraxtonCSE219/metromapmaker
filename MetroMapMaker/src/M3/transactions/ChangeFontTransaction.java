/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.DraggableText;
import javafx.scene.Node;
import javafx.scene.text.Font;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class ChangeFontTransaction implements jTPS_Transaction {
    private DraggableText text;
    private String typeOfEdit;
    private String oldSetting; //used if type edit is font or size
    private String newSetting; //Used if type edit is font or size
    public static final String BOLD = "Bold";
    public static final String ITALICS = "Italics";
    public static final String FONT = "Font";
    public static final String SIZE = "Size";
    
    public ChangeFontTransaction(DraggableText text, String typeOfEdit){
        this.text = text;
        this.typeOfEdit = typeOfEdit;
    }
    
    public ChangeFontTransaction(DraggableText text, String newSetting, String typeOfEdit){
        this.text = text;
        this.typeOfEdit = typeOfEdit; 
        if (typeOfEdit.equals(FONT)){
            oldSetting = text.getFontType();
            this.newSetting = newSetting;
        }
        if (typeOfEdit.equals(SIZE)){
            oldSetting = text.getFontSize();
            this.newSetting = newSetting;
        }
        
    }
    @Override
    public void doTransaction() {
        if (typeOfEdit.equals(BOLD)){
            text.setBoldProperty(!text.getBoldProperty());
        }
        else if (typeOfEdit.equals(ITALICS)){
            text.setItalicsProperty(!text.getItalicsProperty());

        }
        else if (typeOfEdit.equals(FONT)){
            text.setFontType(newSetting);
        }
        else if (typeOfEdit.equals(SIZE)){
            text.setFontSize(newSetting);
        }
    }

    @Override
    public void undoTransaction() {
        if (typeOfEdit.equals(BOLD)){
            text.setBoldProperty(!text.getBoldProperty());
        }
        else if (typeOfEdit.equals(ITALICS)){
            text.setItalicsProperty(!text.getItalicsProperty());

        }
        else if (typeOfEdit.equals(FONT)){
            text.setFontType(oldSetting);
        }
        else if (typeOfEdit.equals(SIZE)){
            text.setFontSize(oldSetting);
        }
    }
    
}
