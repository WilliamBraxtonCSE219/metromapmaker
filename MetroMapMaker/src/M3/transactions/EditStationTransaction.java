/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M3.transactions;

import M3.data.MetroStation;
import jtps.jTPS_Transaction;

/**
 *
 * @author William
 */
public class EditStationTransaction implements jTPS_Transaction{
    private final String typeOfEdit;
    private final MetroStation station;
    private final int index;
    public static final String MOVE = "move";
    public static final String ROTATE = "rotate";
    
    public EditStationTransaction(MetroStation station, String typeOfEdit){
        this.typeOfEdit = typeOfEdit;
        this.station = station;
        if(typeOfEdit.equals(MOVE)){
            index = station.getLabelPosition();
        }
        else if(typeOfEdit.equals(ROTATE)){
            index = station.getRotation();
        }
        else{
            index = 0;
        }
    }
    
    @Override
    public void doTransaction() {
        if(typeOfEdit.equals(MOVE)){
            station.moveLabel();
        }
        if(typeOfEdit.equals(ROTATE)){
            station.rotateLabel();
        }
    }

    @Override
    public void undoTransaction() {
        if(typeOfEdit.equals(MOVE)){
            station.setLabelPosition(index);
        }
        if(typeOfEdit.equals(ROTATE)){
            station.setRotation(index);
        }
    }
    
}
