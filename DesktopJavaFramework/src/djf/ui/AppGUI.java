package djf.ui;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import djf.controller.AppFileController;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_DATA;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javafx.animation.FadeTransition;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.util.Duration;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * This class provides the basic user interface for this application,
 * including all the file controls, but not including the workspace,
 * which would be customly provided for each app.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class AppGUI {
    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    protected AppFileController fileController;
    protected AppTemplate app;
    // THIS IS THE APPLICATION WINDOW
    protected Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    protected Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION AppGUI. NOTE THAT THE WORKSPACE WILL GO
    // IN THE CENTER REGION OF THE appPane
    protected BorderPane appPane;
    
    // THIS IS THE TOP PANE WHERE WE CAN PUT TOOLBAR
    protected FlowPane topToolbarPane;
    
    // THIS IS THE FILE TOOLBAR AND ITS CONTROLS
    protected FlowPane fileToolbar;

    // FILE TOOLBAR BUTTONS
    protected Button newButton;
    protected Button loadButton;
    protected Button saveButton;
    protected Button saveAsButton;
    protected Button exportButton;
    protected Button exitButton;
    
    // THIS DIALOG IS USED FOR GIVING FEEDBACK TO THE USER
    protected AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // THIS TITLE WILL GO IN THE TITLE BAR
    protected String appTitle;
    
    /**
     * This constructor initializes the file toolbar for use.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param initAppTitle The title of this application, which
     * will appear in the window bar.
     * 
     * @param app The app within this gui is used.
     */
    public AppGUI(  Stage initPrimaryStage, 
		    String initAppTitle, 
		    AppTemplate app) throws IOException{
	// SAVE THESE FOR LATER
	primaryStage = initPrimaryStage;
	appTitle = initAppTitle;
	this.app = app;
        // INIT THE TOOLBAR
        initTopToolbar(app);
        		
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow();
        initWelcomeScreen();
        // INIT THE STYLESHEET AND THE STYLE FOR THE FILE TOOLBAR
        initStylesheet(app);
        initFileToolbarStyle();        
    }
    
    /**
     * Accessor method for getting the file toolbar controller.
     */
    public AppFileController getFileController() { return fileController; }
    
    /**
     * Accessor method for getting the application pane, within which all
     * user interface controls are ultimately placed.
     * 
     * @return This application GUI's app pane.
     */
    public BorderPane getAppPane() { return appPane; }
    
    /**
     * Accessor method for getting the toolbar pane in the top, within which
     * other toolbars are placed.
     * 
     * @return This application GUI's app pane.
     */    
    public FlowPane getTopToolbarPane() {
        return topToolbarPane;
    }
    
    /**
     * Accessor method for getting the file toolbar pane, within which all
     * file controls are ultimately placed.
     * 
     * @return This application GUI's app pane.
     */    
    public FlowPane getFileToolbar() {
        return fileToolbar;
    }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     * 
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     * 
     * @return This application's primary stage (i.e. window).
     */    
    public Stage getWindow() { return primaryStage; }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Page has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveButton.setDisable(saved);
        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
	newButton.setDisable(false);
        loadButton.setDisable(false);
	exitButton.setDisable(false);
        saveAsButton.setDisable(false);
        exportButton.setDisable(false);
        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /****************************************************************************/
    
     private void initWelcomeScreen() throws IOException {
        Stage welcomeScreen = app.getWelcomeScreen();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        VBox welcomePane = new VBox(20);
        welcomePane.setPadding(new Insets(20, 20, 20, 20));
        //welcomePane.setAlignment(Pos.CENTER);
        VBox buttonHolder = new VBox(5);
        StackPane logoHolder = new StackPane();
        logoHolder.setAlignment(Pos.CENTER);
        buttonHolder.setAlignment(Pos.CENTER);
        welcomePane.getChildren().add(logoHolder);
        welcomePane.getChildren().add(buttonHolder);
        Scene welcomeScene = new Scene(welcomePane, 500,500);
        welcomeScreen.initModality(Modality.APPLICATION_MODAL);
        
        //MAKE FADE IN
        FadeTransition ft = new FadeTransition(Duration.millis(1500), welcomePane);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();
        
        //SET EVERYTHING
        welcomeScreen.setScene(welcomeScene);
        welcomeScreen.setTitle(props.getProperty(APP_TITLE) + " Welcome Screen");
        welcomeScreen.setScene(welcomeScene);
        ImageView logo = new ImageView(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO));
        logoHolder.getChildren().add(logo);
        
        //MAKE THE BUTTONS
        Button newFile = new Button("Create New Map"); 
        buttonHolder.getChildren().add(newFile);
        newFile.setOnAction(e -> {
            fileController.handleNewRequest();
            welcomeScreen.close();
        });
        
        //LOAD RECENT MAPS FROM JSON
        Label recentMapsLabel = new Label ("Or load a recent map!");
        buttonHolder.getChildren().add(recentMapsLabel);
        try{
            JsonObject json = fileController.loadJSONRecentMapsFile(PATH_DATA + "recentFiles");
            JsonArray jsonMapArray = json.getJsonArray("RecentFiles");
            for(int i = 0; i < 10 && i < jsonMapArray.size(); i++){
                JsonObject fileObject = jsonMapArray.getJsonObject(i);
                //File fileObjectPath  = new File(fileObject.getString("FilePath"));
                String fileName = fileObject.getString("FileName");
                File checkFile = new File(fileObject.getString("FilePath") + " Metro" + props.getProperty(WORK_FILE_EXT));
                if (checkFile.exists()){
                    Hyperlink recentFileButton = new Hyperlink(fileName);
                    buttonHolder.getChildren().add(recentFileButton);
                    recentFileButton.setOnAction(e -> {
                        fileController.loadDataPass(fileObject.getString("FilePath"));
                        welcomeScreen.close();
                    });
                }
            }
        }
        catch(IOException ioe){
            //Do nothing. This doesnt hurt us
        }
     }
   
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initTopToolbar(AppTemplate app) {
        fileToolbar = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newButton = initChildButton(fileToolbar,	NEW_ICON.toString(),	    NEW_TOOLTIP.toString(),	false);
        loadButton = initChildButton(fileToolbar,	LOAD_ICON.toString(),	    LOAD_TOOLTIP.toString(),	false);
        saveButton = initChildButton(fileToolbar,	SAVE_ICON.toString(),	    SAVE_TOOLTIP.toString(),	true);
        saveAsButton = initChildButton(fileToolbar,	SAVE_AS_ICON.toString(),     SAVE_TOOLTIP.toString(),	true);
        exportButton = initChildButton(fileToolbar,	EXPORT_ICON.toString(),     EXPORT_TOOLTIP.toString(),	true);
        exitButton = initChildButton(fileToolbar,	EXIT_ICON.toString(),	    EXIT_TOOLTIP.toString(),	false);

	// AND NOW SETUP THEIR EVENT HANDLERS
        fileController = new AppFileController(app);
        newButton.setOnAction(e -> {
            fileController.handleNewRequest();
        });
        loadButton.setOnAction(e -> {
            fileController.handleLoadRequest();
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        saveAsButton.setOnAction(e -> {
            fileController.handleSaveAsRequest();
        });
        exportButton.setOnAction(e -> {
            fileController.handleExportRequest();
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });	
        
        // NOW PUT THE FILE TOOLBAR IN THE TOP TOOLBAR, WHICH COULD
        // ALSO STORE OTHER TOOLBARS
        topToolbarPane = new FlowPane();
        topToolbarPane.getChildren().add(fileToolbar);
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    private void initWindow() {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // SET THE WINDOW TITLE
        primaryStage.setTitle(appTitle);

        // START FULL-SCREEN OR NOT, ACCORDING TO PREFERENCES
        primaryStage.setMaximized("true".equals(props.getProperty(START_MAXIMIZED)));

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setTop(topToolbarPane);
        primaryScene = new Scene(appPane);

        // SET THE APP PANE PREFERRED SIZE ACCORDING TO THE PREFERENCES
        double prefWidth = Double.parseDouble(props.getProperty(PREF_WIDTH));
        double prefHeight = Double.parseDouble(props.getProperty(PREF_HEIGHT));
        appPane.setPrefWidth(prefWidth);
        appPane.setPrefHeight(prefHeight);

        // SET THE APP ICON
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        primaryStage.getIcons().add(new Image(appIcon));

        // NOW TIE THE SCENE TO THE WINDOW
        primaryStage.setScene(primaryScene);
    }
    
    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     * 
     * @param toolbar Toolbar pane into which to place this button.
     * 
     * @param icon Icon image file name for the button.
     * 
     * @param tooltip Tooltip to appear when the user mouses over the button.
     * 
     * @param disabled true if the button is to start off disabled, false otherwise.
     * 
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
     /**
     * This is a public helper method for initializing a simple button with
     * text and tooltip and placing it into a toolbar.
     * 
     * @param toolbar Toolbar pane into which to place this button.
     * 
     * @param text Text to appear on the button
     * 
     * @param tooltip Tooltip to appear when the user mouses over the button.
     * 
     * @param disabled true if the button is to start off disabled, false otherwise.
     * 
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initChildButtonText(Pane toolbar, String text, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setText(props.getProperty(text));
        button.setAlignment(Pos.CENTER);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
     public ComboBox initComboBox(Pane toolbar, ObservableList<?> families, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

	// MAKE THE COMBOBOX
        ComboBox comboBox = new ComboBox(families);
        comboBox.setDisable(disabled);
        Tooltip comboTooltip = new Tooltip(props.getProperty(tooltip));
        comboBox.setTooltip(comboTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(comboBox);
	
	// AND RETURN THE COMPLETED BUTTON
        return comboBox;
    }
     
    public ChoiceBox initChoiceBox(Pane toolbar, ObservableList<?> families, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

	// MAKE THE COMBOBOX
        ChoiceBox choiceBox = new ChoiceBox(families);
        choiceBox.setDisable(disabled);
        Tooltip comboTooltip = new Tooltip(props.getProperty(tooltip));
        choiceBox.setTooltip(comboTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(choiceBox);
	
	// AND RETURN THE COMPLETED BUTTON
        return choiceBox;
    }
       public ComboBox initComboBox(Pane toolbar, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

	// MAKE THE COMBOBOX
        ComboBox comboBox = new ComboBox();
        comboBox.setDisable(disabled);
        Tooltip comboTooltip = new Tooltip(props.getProperty(tooltip));
        comboBox.setTooltip(comboTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(comboBox);
	
	// AND RETURN THE COMPLETED BUTTON
        return comboBox;
    }
    
       public CheckBox initCheckBox(Pane toolbar, String tooltip) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

	// MAKE THE COMBOBOX
        CheckBox checkBox = new CheckBox();
        Tooltip checkBoxTooltip = new Tooltip(props.getProperty(tooltip));
        checkBox.setTooltip(checkBoxTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(checkBox);
	
	// AND RETURN THE COMPLETED BUTTON
        return checkBox;
    }
     
    public Label initLabel(Pane toolbar, String text) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE	
	// NOW MAKE THE BUTTON
        Label label = new Label();
        label.setText(props.getProperty(text));	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(label);
	
	// AND RETURN THE COMPLETED BUTTON
        return label;
    }
    
    public Slider initSlider(Pane toolbar){
         PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE	
	// NOW MAKE THE BUTTON
        Slider slider = new Slider();
       
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(slider);
	
	// AND RETURN THE COMPLETED BUTTON
        return slider;
    }
    /**
     * 
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar. But makes a toggleButton
     * 
     * @param toolbar Toolbar pane into which to place this button.
     * 
     * @param icon Icon image file name for the button.
     * 
     * @param tooltip Tooltip to appear when the user mouses over the button.
     * 
     * @param disabled true if the button is to start off disabled, false otherwise.
     * 
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
     public ToggleButton initChildToggleButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        ToggleButton button = new ToggleButton();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
   /**
     *  Note that this is the default style class for the top file toolbar
     * and that style characteristics for this type of component should be 
     * put inside app_properties.xml.
     */
    public static final String CLASS_BORDERED_PANE = "bordered_pane";

   /**
     *  Note that this is the default style class for the file buttons in
     * the top file toolbar and that style characteristics for this type
     * of component should be put inside app_properties.xml.
     */
    public static final String CLASS_FILE_BUTTON = "file_button";
    
    /**
     * This function sets up the stylesheet to be used for specifying all
     * style for this application. Note that it does not attach CSS style
     * classes to controls, that must be done separately.
     */
    private void initStylesheet(AppTemplate app) {
	// SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(APP_PATH_CSS);
	stylesheet += props.getProperty(APP_CSS);
        Class appClass = app.getClass();
	URL stylesheetURL = appClass.getResource(stylesheet);
	String stylesheetPath = stylesheetURL.toExternalForm();
	primaryScene.getStylesheets().add(stylesheetPath);	
    }
    
    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    private void initFileToolbarStyle() {
	topToolbarPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
	newButton.getStyleClass().add(CLASS_FILE_BUTTON);
	loadButton.getStyleClass().add(CLASS_FILE_BUTTON);
	saveButton.getStyleClass().add(CLASS_FILE_BUTTON);
	exitButton.getStyleClass().add(CLASS_FILE_BUTTON);
    }
    
    /**
     * this function refreshes to toolTips of the toptoolbar, as well as the title
     */
    public void refreshToolBarText(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        primaryStage.setTitle(props.getProperty(APP_TITLE));
        newButton.setTooltip(new Tooltip(props.getProperty(NEW_TOOLTIP)));
        loadButton.setTooltip(new Tooltip(props.getProperty(LOAD_TOOLTIP)));
        saveButton.setTooltip(new Tooltip(props.getProperty(SAVE_TOOLTIP)));
        exitButton.setTooltip(new Tooltip(props.getProperty(EXIT_TOOLTIP)));
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
	messageDialog.refreshLabels();
	AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
	yesNoDialog.refreshLabels();

}

   
}
